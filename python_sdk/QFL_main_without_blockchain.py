import os
import copy
import torch
import torch.nn as nn
from python_sdk.QFL.data_utils import DatasetSplit
from python_sdk.QFL.initialize_data import load_model_and_data_and_userGroup
from torch.utils.data import DataLoader
import pickle
import matplotlib.pyplot as plt
import datetime
import time

import logging
logger = logging.getLogger(__name__)
LOG_INTERVAL = 50


seed = 0
# rn.seed(seed)     #should be command when use the initialize the data
# np.random.seed(seed)   #should be command when use the initialize the data
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

class Arguments:
    def __init__(self):
        self.batch_size = 10
        self.test_batch_size = 100
        self.lr = 0.005
        self.device = 'cpu'
        self.local_epoch = 2
        self.lr_decay = 0.996
        self.momentum = 0.9

        self.iid = False  # iid: True; non-iid: False
        self.num_bit = 32  # number of bits to represent the quantized gradient, when num_bit

        self.num_users = 30
        self.epoch = 200  # training round at this time.
        self.continue_train = False  # If True, it will continue training the last taskid.
        self.start_round = 0

        self.model_type = 'withoutQS'   # withoutQS  withQ  withS withQS
        self.dataset_name = 'mnist'  # mnist cifar  traffic
        self.net_name = "CNN"        # CNN    LeNet   NetCNN
        self.log_folder_name = "QFL/log_mnist_without_blockchain_withoutQS"


class Client:
    dataset = None
    criterion = nn.CrossEntropyLoss()

    def __init__(self, args, data_idx, client_id):

        self.local_epoch = args.local_epoch
        self.batch_size = args.batch_size
        self.lr = args.lr
        self.lr_decay = args.lr_decay
        self.device = args.device
        self.momentum = args.momentum

        self.trainloader = DataLoader(DatasetSplit(self.dataset, data_idx),
                                      batch_size=self.batch_size, shuffle=True)
        self.client_id = client_id
        self.num_data = len(data_idx)

    def local_update_upload(self, local_model, num_bit, cr):

        initial = copy.deepcopy(local_model)  # copy the model distributed from the server

        lr = self.lr * (self.lr_decay ** cr)  # decay the learning rate
        optimizer = torch.optim.SGD(local_model.parameters(), lr=lr, momentum=self.momentum)
        local_model.train()
        for e in range(self.local_epoch):
            for batch_idx, (images, labels) in enumerate(self.trainloader):
                images, labels = images.to(self.device), labels.to(self.device)
                local_model.zero_grad()
                output = local_model(images)
                loss = self.criterion(output, labels)
                loss.backward()
                optimizer.step()

        # ==============================================================================
        # Client quantizes the gradient, and save the quantized grad $G_{t}$ in the disk again
        local_model.quantize_grad_without_blockchain(initial, num_bit, fn='grad' + str(self.client_id))
        # ==============================================================================

        print('\rClient %d finished local training at %d communication round' % (self.client_id, cr), end='')
        return self.num_data


class Server:
    def __init__(self, args, model, clients, testset, log_name):
        self.model_dist = model
        self.model_glob = copy.deepcopy(self.model_dist)
        self.clients = clients
        self.testloader = DataLoader(testset, batch_size=args.test_batch_size, shuffle=False)
        self.criterion = nn.CrossEntropyLoss()
        self.device = args.device

        # Note that only the global test loss and accuracy are recored in this demo
        self.his_loss = []
        self.his_acc = []
        self.timeslot = []

        # path to save the info
        if not os.path.exists(args.log_folder_name):
            os.mkdir(args.log_folder_name)
        self.log_path = args.log_folder_name + log_name

    def _single_client(self, client_id, num_bit, cr):
        """
        Perform local update to single client at one communication round.
        """
        local_model = copy.deepcopy(self.model_dist)
        num_local_data = self.clients[client_id].local_update_upload(local_model, num_bit, cr)

        # =======================================================================================================
        # The server loads the gradient $G_{t}$ from the disk, and obtains $W_{t} = W_{t-1} + G_{t}$
        local_model.decode_grad_without_blockchain(copy.deepcopy(self.model_dist), fn='grad' + str(self.clients[client_id].client_id))
        self.model_glob.aggregate(local_model, num_local_data)
        # =======================================================================================================

    def single_cr(self, num_bit, cr):
        """
        Perform local update to all the clients one by one。
        """
        curr = time.time()

        for client_id in range(len(self.clients)):
            self._single_client(client_id, num_bit, cr)

        self.model_dist = copy.deepcopy(self.model_glob)

        # ============================================ #
        # clear the aggregation data stored in the model
        self.model_dist.zero_agg_data()
        self.model_glob.zero_agg_data()
        # ============================================ #

        self.timeslot.append(time.time() - curr)

        torch.save(self.model_dist, 'model.pt')

        # evaluate the global model
        self.model_dist.eval()
        test_loss = 0
        correct = 0
        total = 0
        with torch.no_grad():
            for batch_idx, (images, labels) in enumerate(self.testloader):
                images, labels = images.to(self.device), labels.to(self.device)
                outputs = self.model_dist(images)
                loss = self.criterion(outputs, labels)

                test_loss += loss.item()
                _, predicted = outputs.max(1)
                total += labels.size(0)
                correct += predicted.eq(labels).sum().item()
        acc = 100. * correct / total
        test_loss /= total
        self.his_acc.append(acc)
        self.his_loss.append(test_loss)

        # write the log info
        with open(self.log_path, 'wb') as file:
            pickle.dump([self.his_acc, self.his_loss, self.timeslot], file)
        print("\nComm round %d, test acc: %.6f, test loss: %.6f" % (cr, acc, test_loss))
        return acc, test_loss


def main():
    start = datetime.datetime.now()
    args = Arguments()
    clients = []
    # when compare with blockchain version, should use the same training data, user_group, and same initialize model file.
    # -------------------------------------------------------------------------------------------------------------------
    file_path = './QFL/init_data_mnist/{}_{}_{}_idd{}_num_users{}_initial_data_and_userGroup'.format(args.model_type, args.dataset_name, args.net_name, args.iid, args.num_users)
    train_dataset, test_dataset, user_groups = load_model_and_data_and_userGroup(file_path)
    Client.dataset = train_dataset
    model = torch.load('./QFL/init_data_mnist/{}_{}_{}_idd{}_num_users{}_initial_model.pt'.format(args.model_type, args.dataset_name, args.net_name, args.iid, args.num_users))
    # -------------------------------------------------------------------------------------------------------------------

    # model = build_model(args)
    # train_dataset, test_dataset, user_groups = get_dataset(args.dataset_name, args.iid, args.num_users)
    # Client.dataset = train_dataset

    if args.continue_train:
        model= torch.load('model.pt')

    for user_idx in range(args.num_users):
        clients.append(Client(args, user_groups[user_idx], user_idx))

    server = Server(args, model, clients, test_dataset,
                    log_name='/{}_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}'.format(args.net_name, args.dataset_name,
                                                                                        args.model_type, args.iid,
                                                                                        args.num_bit, args.num_users,
                                                                                        args.lr, args.epoch, args.start_round, (args.start_round + args.epoch -1)))
    for cr in range(args.epoch):
        server.single_cr(args.num_bit, cr)

    end = datetime.datetime.now()
    print('Running time:%s Seconds' % (end - start))

    with open(args.log_folder_name + '/time', 'wb') as file:
        pickle.dump(['{}_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}'.format(args.net_name, args.dataset_name, args.model_type, args.iid, args.num_bit,
                                                                                args.num_users, args.lr, args.epoch, args.start_round, (args.start_round + args.epoch -1)),
                     'Running time:%s Seconds' % (end - start)], file)

    data = pickle.load(open(args.log_folder_name +
        '/{}_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}'.format(args.net_name, args.dataset_name, args.model_type,
                                                                                                args.iid,
                                                                                                args.num_bit,
                                                                                                args.num_users, args.lr,
                                                                                                args.epoch, args.start_round, (args.start_round + args.epoch -1)), 'rb'))

    plt.figure()
    plt.title('Average Accuracy vs Communication rounds')
    plt.plot(range(len(data[0])), data[0])
    plt.ylabel('train_accuracy')
    plt.savefig(args.log_folder_name + '/{}_{}_accuracy_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}.png'.format(
        args.net_name, args.dataset_name, args.model_type, args.iid, args.num_bit, args.num_users, args.lr, args.epoch, args.start_round, (args.start_round + args.epoch -1)))

    plt.figure()
    plt.title('Training Loss vs Communication rounds')
    plt.plot(range(len(data[1])), data[1])
    plt.ylabel('train_loss')
    plt.savefig(args.log_folder_name +
        '/{}_{}_loss_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}.png'.format(args.net_name,
                                                                                                         args.dataset_name,
                                                                                                         args.model_type,
                                                                                                         args.iid,
                                                                                                         args.num_bit,
                                                                                                         args.num_users,
                                                                                                         args.lr,
                                                                                                         args.epoch, args.start_round, (args.start_round + args.epoch -1)))

    plt.figure()
    plt.title('Running time vs Communication rounds')
    plt.plot(range(len(data[2][1:])), data[2][1:])
    plt.ylabel('running time')
    plt.savefig(args.log_folder_name +
        '/{}_{}_time_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}.png'.format(args.net_name,
                                                                                                         args.dataset_name,
                                                                                                         args.model_type,
                                                                                                         args.iid,
                                                                                                         args.num_bit,
                                                                                                         args.num_users,
                                                                                                         args.lr,
                                                                                                         args.epoch, args.start_round, (args.start_round + args.epoch -1)))


if __name__ == "__main__":
    main()

