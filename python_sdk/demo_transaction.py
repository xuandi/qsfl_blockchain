#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
  FISCO BCOS/Python-SDK is a python client for FISCO BCOS2.0 (https://github.com/FISCO-BCOS/)
  FISCO BCOS/Python-SDK is free software: you can redistribute it and/or modify it under the
  terms of the MIT License as published by the Free Software Foundation. This project is
  distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Thanks for
  authors and contributors of eth-abi, eth-account, eth-hash，eth-keys, eth-typing, eth-utils,
  rlp, eth-rlp , hexbytes ... and relative projects
  @author: kentzhang
  @date: 2019-06
'''
from python_sdk.client.contractnote import ContractNote
from python_sdk.client.bcosclient import BcosClient
import os
from python_sdk.eth_utils import to_checksum_address
from python_sdk.client.datatype_parser import DatatypeParser
from python_sdk.client.common.compiler import Compiler
from python_sdk.client.bcoserror import BcosException, BcosError
from python_sdk.client_config import client_config
import sys
import traceback
import time
import pickle
import matplotlib.pyplot as plt


if os.path.isfile(client_config.solc_path) or os.path.isfile(client_config.solcjs_path):
    Compiler.compile_file("contracts/ModelStorageMain.sol")
abi_file = "contracts/ModelStorageMain.abi"
data_parser = DatatypeParser()
data_parser.load_abi_file(abi_file)
contract_abi = data_parser.contract_abi
client = BcosClient()
print(client.getinfo())
# 部署合约
print("\n>>Deploy:----------------------------------------------------------")
with open("contracts/ModelStorageMain.bin", 'r') as load_f:
    contract_bin = load_f.read()
    load_f.close()
result = client.deploy(contract_bin)
print("deploy", result)
print("new address : ", result["contractAddress"])
contract_name = os.path.splitext(os.path.basename(abi_file))[0]
memo = "tx:" + result["transactionHash"]
# 把部署结果存入文件备查
ContractNote.save_address_to_contract_note(contract_name,
                                           result["contractAddress"])
# 发送交易，调用一个改写数据的接口
print("\n>>sendRawTransaction:----------------------------------------------------")
to_address = result['contractAddress']  # use new deploy address

def writeData(client, to_address,args):
    try:
        receipt = client.sendRawTransactionGetReceipt(to_address, contract_abi, "recordModel", args)
        print("blockNumber:", int(receipt['blockNumber'],16))

    except BcosException as e:
        print("execute demo_transaction failed ,BcosException for: {}".format(e))
        traceback.print_exc()
    except BcosError as e:
        print("execute demo_transaction failed ,BcosError for: {}".format(e))
        traceback.print_exc()
    except Exception as e:
        client.finish()
        traceback.print_exc()
    # client.finish()
    # sys.exit(0)

model_str = str(pickle.load(open('/home/li/Desktop/qsfl_blockchain/python_sdk/QFL/grad_base/grad0', 'rb')))
print(sys.getsizeof(model_str))
time_list = []

for i in range(500):
    args = [to_checksum_address('0x7029c502b4F824d19Bd7921E9cb74Ef92392FB1c'), str(i), model_str]
    curr = time.time()
    writeData(client, to_address, args)
    time_list.append(time.time() - curr)
client.finish()
with open('demo_transaction_time_4_client_outside', 'wb') as file:
    pickle.dump([time_list], file)
data = pickle.load(open('demo_transaction_time_4_client_outside', 'rb'))
plt.title('Running time')
plt.plot(range(len(data[0])), data[0], linewidth=1)
plt.show()