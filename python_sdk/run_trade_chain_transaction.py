import sys
sys.path.append('../')
import time
import pickle
import matplotlib.pyplot as plt
from python_sdk.contract_api import SmartContract, get_contract_addr, get_account_addr
from python_sdk.eth_utils import to_checksum_address
from python_sdk.contract_api import Task, get_account_addr
from python_sdk.client.bcosclient import BcosClient

def trade_transfer_token(fiscoClient):
    task_contract = Task()
    task_id = task_contract.create_new_task(fiscoClient, 100, 0, int(0.005 * 1000))
    smart_contract = SmartContract('TradeMain')
    task_publisher = get_account_addr('bin/accounts/accounts.ini', ['task_publisher'])
    buyer1 = get_account_addr('bin/accounts/accounts.ini', ['worker1'])
    buyer2 = get_account_addr('bin/accounts/accounts.ini', ['worker2'])
    task_publisher_addr = to_checksum_address(task_publisher[0])
    buyer1_addr = to_checksum_address(buyer1[0])
    buyer2_addr = to_checksum_address(buyer2[0])
    smart_contract.write_data(fiscoClient, 'createOrder', [task_publisher_addr, task_id, 'global_model', 100])
    use_time1 = []
    use_time2 = []
    for i in range(2500):
        print(i)
        curr1 = time.time()
        smart_contract.write_data(fiscoClient, 'purchaseModel', [buyer1_addr, task_id])
        use_time1.append(time.time() - curr1)
        curr2 = time.time()
        smart_contract.write_data(fiscoClient, 'purchaseModel', [buyer2_addr, task_id])
        use_time2.append(time.time() - curr2)
    with open('transaction_trade_chain_time', 'wb') as file:
        pickle.dump([use_time1, use_time2], file)

def read_balance(fiscoClient):
    smart_contract = SmartContract('TradeToken')
    task_publisher = get_account_addr('bin/accounts/accounts.ini', ['task_publisher'])
    worker1 = get_account_addr('bin/accounts/accounts.ini', ['worker1'])
    worker2 = get_account_addr('bin/accounts/accounts.ini', ['worker2'])
    task_publisher_addr = to_checksum_address(task_publisher[0])
    worker1_addr = to_checksum_address(worker1[0])
    worker2_addr = to_checksum_address(worker2[0])
    worker1_balance = smart_contract.get_data_with_args(fiscoClient,'balanceOf', [worker1_addr])
    print('worker1_balance:')
    print(worker1_balance)
    worker2_balance = smart_contract.get_data_with_args(fiscoClient, 'balanceOf', [worker2_addr])
    print('worker2_balance:')
    print(worker2_balance)
    task_publisher_balance = smart_contract.get_data_with_args(fiscoClient, 'balanceOf', [task_publisher_addr])
    print('task_publisher_balance:')
    print(task_publisher_balance)

client = BcosClient()

trade_transfer_token(client)
read_balance(client)

client.finish()
data = pickle.load(open('transaction_trade_chain_time', 'rb'))
plt.title('Running time')
plt.plot(range(len(data[0])), data[0], linewidth=1)
print(data[0])
plt.plot(range(len(data[1])), data[1], linewidth=1)
plt.savefig("transaction_trade_chain_time2.png")
plt.show()