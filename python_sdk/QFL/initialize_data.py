import pickle
import os
import torch
import random as rn
import numpy as np
from python_sdk.QFL.models_withQS import CNN_mnist_withQS, CNN_cifar_withQS, LeNet_traffic_withQS
from python_sdk.QFL.models_withQ import CNN_mnist_withQ, CNN_cifar_withQ, LeNet_traffic_withQ
from python_sdk.QFL.models_withS import CNN_mnist_withS, CNN_cifar_withS, LeNet_traffic_withS
from python_sdk.QFL.models_base import CNN_mnist_base, CNN_cifar_base, LeNet_traffic_base, NetCNN
from python_sdk.QFL.data_utils import DatasetSplit, get_dataset


seed = 0
# rn.seed(seed)    #should be command after initialize the data
# np.random.seed(seed)  #should be command after initialize the data
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

def build_model(args):
    # build model
    if args.model_type == 'withoutQS':
        if args.dataset_name == 'cifar':
            net_glob = CNN_cifar_base().to(args.device)
        elif args.dataset_name == 'mnist':
            net_glob = CNN_mnist_base().to(args.device)
            # net_glob = NetCNN().to(args.device)
        elif args.dataset_name == 'traffic':
            print('model type: LeNet_traffic_base')
            net_glob = LeNet_traffic_base().to(args.device)
        else:
            exit('Error: unrecognized model')
        return net_glob

    elif args.model_type == 'withQ':
        if args.dataset_name == 'cifar':
            net_glob = CNN_cifar_withQ().to(args.device)
        elif args.dataset_name == 'mnist':
            net_glob = CNN_mnist_withQ().to(args.device)
        elif args.dataset_name == 'traffic':
            print('model type: LeNet_traffic_withQ')
            net_glob = LeNet_traffic_withQ().to(args.device)
        else:
            exit('Error: unrecognized model')
        return net_glob

    elif args.model_type == 'withS':
        if args.dataset_name == 'cifar':
            net_glob = CNN_cifar_withS().to(args.device)
        elif args.dataset_name == 'mnist':
            net_glob = CNN_mnist_withS().to(args.device)
        elif args.dataset_name == 'traffic':
            print('model type: LeNet_traffic_withS')
            net_glob = LeNet_traffic_withS().to(args.device)
        else:
            exit('Error: unrecognized model')
        return net_glob

    elif args.model_type == 'withQS':
        if args.dataset_name == 'cifar':
            net_glob = CNN_cifar_withQS().to(args.device)
        elif args.dataset_name == 'mnist':
            net_glob = CNN_mnist_withQS().to(args.device)
        elif args.dataset_name == 'traffic':
            print('model type: LeNet_traffic_withQS')
            net_glob = LeNet_traffic_withQS().to(args.device)
        else:
            exit('Error: unrecognized model')
        return net_glob

    else:
        exit('Error: unrecognized model type')


def init_model_and_data_and_userGroup(args):
    if not os.path.exists('init_data_mnist/'):
        os.mkdir('init_data_mnist/')
    train_dataset, test_dataset, user_groups = get_dataset(args.dataset_name, args.iid, args.num_users)
    model = build_model(args)
    torch.save(model, 'init_data_mnist/{}_{}_{}_idd{}_num_users{}_initial_model.pt'.format(args.model_type, args.dataset_name, args.net_name, args.iid, args.num_users))
    file_path = 'init_data_mnist/{}_{}_{}_idd{}_num_users{}_initial_data_and_userGroup'.format(args.model_type, args.dataset_name, args.net_name, args.iid, args.num_users)
    with open(file_path, 'wb') as file:
        pickle.dump([train_dataset, test_dataset, user_groups], file)

def load_model_and_data_and_userGroup(file_path):
    train_dataset, test_dataset, user_groups = pickle.load(open(file_path, 'rb'))
    return train_dataset, test_dataset, user_groups


class Arguments:
    def __init__(self):
        self.batch_size = 10
        self.test_batch_size = 100
        self.lr = 0.005
        self.device = 'cpu'
        self.local_epoch = 2
        self.lr_decay = 0.996
        self.momentum = 0.9

        self.iid = False  # iid: True; non-iid: False
        self.num_bit = 32 # number of bits to represent the quantized gradient, when num_bit

        self.num_users = 100
        self.epoch = 100  # training round at this time.
        self.continue_train = False  # If True, it will continue training the last taskid.
        self.start_round = 0

        self.model_type = 'withoutQS'   # withoutQS  withQ  withS  withQS
        self.dataset_name = 'mnist'  # mnist cifar  traffic
        self.net_name = "CNN"        # CNN  LeNet NetCNN
        self.log_folder_name = "QFL/log_without_blockchain_withQS"


# should be command after initialize the data
# args = Arguments()  #should be command after initialize the data
# init_model_and_data_and_userGroup(args) #should be command after initialize the data
