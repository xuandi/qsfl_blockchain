import pickle
import matplotlib.pyplot as plt
import math
import numpy as np


# only trading transaction:
only_trade_chain_1000transactions = round(2576 * 4 / 4 /1024, 2)
only_trade_chain_5000transactions = round((11152 + 10624 + 11136 + 10632) / 4 /1024, 2)

# Base line, without quantization and topK, each task train 100 or 40 rounds.
baseline_1Task = round((2100224 + 2152944 + 2140176 + 2119168) / 4 /1024, 2)
baseline_2Task = round((4328288 + 4221280 + 4231504 + 4179296) / 4 /1024, 2)
baseline_1Task_40Rounds = round((918856 + 936768 + 916784 + 936752) / 4 /1024, 2)
baseline_2Task_40Rounds = round((1861088 + 1886176 + 1777632 + 1883616) / 4 /1024, 2)
baseline_2Task_with_2000Trans = round(((4328288 + 4221280 + 4231504 + 4179296) / 4 /1024 + 2 * only_trade_chain_1000transactions), 2)


# only with quantization, each task train 100 or 40 rounds: conv=4 bit; linear=6 bit; grad_size=195.021 KB
only_Quand_1Task = round((369672 + 383512 + 387072 + 386568) / 4 /1024, 2)
only_Quand_2Task = round((767296 + 756032 + 761648 + 761664) / 4 /1024, 2)
only_Quand_1Task_40Rounds = round((159024 + 160576 + 163120 + 162112) /4 /1024, 2)


# only with topK, each task train 100 or 40 rounds: conv=0.6(select 40% parameters); linear=0.8 (select 20% parameters); grad_size=268.8 KB
only_Topk_1Task = round((726008 + 738312 + 732672 + 755704) / 4 /1024, 2)
only_Topk_2Task = round((1480032 + 1454928 + 1436464 + 1475920) / 4 /1024, 2)
only_Topk_1Task_40Rounds = round((311600 + 314184 + 311104 + 317232) /4 /1024, 2)


# with quantization and topK, each task train 100 or 40 rounds: Quand: conv=4 bit; linear=6 bit; TopK:conv=0.6; linear=0.8; grad_size=47.3 KB
quand_and_Topk_1Task = round((283632 + 288264 + 284696 + 280064) / 4 /1024, 2)
quand_and_Topk_2Task = round((586144 + 564144 + 561568 + 585648) / 4 /1024, 2)
quand_and_Topk_1Task_40Rounds = round((119112 + 118080 + 120112 + 121144) / 4 /1024, 2)

quand_and_Topk_2Task_with_2000Trans = round(((586144 + 564144 + 561568 + 585648) / 4 /1024+ 2 * only_trade_chain_1000transactions), 2)


list = [only_trade_chain_5000transactions, only_trade_chain_5000transactions, only_Topk_1Task_40Rounds, only_Quand_1Task_40Rounds, quand_and_Topk_1Task_40Rounds]
list2 = [0, baseline_1Task_40Rounds, 0, 0, 0]
list2_3= [only_trade_chain_5000transactions, (only_trade_chain_5000transactions + baseline_1Task_40Rounds), only_Topk_1Task_40Rounds, only_Quand_1Task_40Rounds, quand_and_Topk_1Task_40Rounds]
list3 = [0, (baseline_2Task_40Rounds - baseline_1Task_40Rounds), 0, 0, 0]

plt.title('Storage cost compare')
labels = ['5000 Trans', 'baseline', 'with Top-k', 'with Quand', 'with Quand&top-k']
index = np.arange(len(labels))
plt.bar(index, list, align='center')
plt.bar(index, list2, align='center', bottom=list)
plt.bar(index, list3, align='center', bottom=list2_3)
plt.xticks(index, labels)

for a, b in zip(index, list):
    plt.text(a, b + 0.05, str(b)+'MB', ha='center', va='bottom')
    if a == 1:
        plt.text(a, baseline_1Task_40Rounds * 0.5, 'task1\n('+str(round(baseline_1Task_40Rounds, 2))+')MB', ha='center', va='bottom', color='white')
        plt.text(a, (baseline_1Task_40Rounds + only_trade_chain_5000transactions) +0.05, str(round((baseline_1Task_40Rounds + only_trade_chain_5000transactions), 2))+'MB', ha='center', va='bottom')
        plt.text(a, baseline_2Task_40Rounds * 0.75, 'task2\n('+str(round((baseline_2Task_40Rounds - baseline_1Task_40Rounds),2))+')MB', ha='center', va='bottom', color='white')
        plt.text(a, (baseline_2Task_40Rounds + only_trade_chain_5000transactions +0.05), str(round((baseline_2Task_40Rounds + only_trade_chain_5000transactions), 2))+'MB', ha='center', va='bottom')

plt.ylabel('Storage cost(MB)')


plt.show()