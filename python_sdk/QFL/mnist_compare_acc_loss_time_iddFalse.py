import pickle
import matplotlib.pyplot as plt
from numpy import *

data_base_16num = pickle.load(open(
    "log_mnist_without_blockchain_withoutQS/CNN_mnist_withoutQS_iidFalse_32num_bit_16num_users_0.005lr_200epoch0to199", 'rb'))
acc_base_16num = data_base_16num[0]
loss_base_16num = data_base_16num[1]
time_base_16num = data_base_16num[2]
time_mean_base_16num = mean(time_base_16num)

data_base_20num = pickle.load(open(
    "log_mnist_without_blockchain_withoutQS/CNN_mnist_withoutQS_iidFalse_32num_bit_20num_users_0.005lr_200epoch0to199", 'rb'))
acc_base_20num = data_base_20num[0]
loss_base_20num = data_base_20num[1]
time_base_20num = data_base_20num[2]
time_mean_base_20num = mean(time_base_20num)

data_base_50num = pickle.load(open(
    "log_mnist_without_blockchain_withoutQS/CNN_mnist_withoutQS_iidFalse_32num_bit_50num_users_0.005lr_200epoch0to199", 'rb'))
acc_base_50num = data_base_50num[0]
loss_base_50num = data_base_50num[1]
time_base_50num = data_base_50num[2]
time_mean_base_50num = mean(time_base_50num)

data_base_100num = pickle.load(open(
    "log_mnist_without_blockchain_withoutQS/CNN_mnist_withoutQS_iidFalse_32num_bit_100num_users_0.005lr_200epoch0to199", 'rb'))
acc_base_100num = data_base_100num[0]
loss_base_100num = data_base_100num[1]
time_base_100num = data_base_100num[2]
time_mean_base_100num = mean(time_base_100num)


data_base_20num = pickle.load(open(
    "log_mnist_without_blockchain_withoutQS/CNN_mnist_withoutQS_iidFalse_32num_bit_20num_users_0.005lr_200epoch0to199", 'rb'))
acc_base_20num = data_base_20num[0]
loss_base_20num = data_base_20num[1]
time_base_20num = data_base_20num[2]
time_mean_base_20num = mean(time_base_20num)

data_withQ = pickle.load(open(
    "log_mnist_without_blockchain_withQ_conv3bit_linear2bit/CNN_mnist_withQ_iidFalse_32num_bit_20num_users_0.005lr_300epoch0to299", 'rb'))
acc_withQ = data_withQ[0][0:200]
loss_withQ = data_withQ[1][0:200]
time_withQ = data_withQ[2][0:200]
time_mean_withQ = mean(time_withQ)

data_withQ_2bit = pickle.load(open(
    "log_mnist_without_blockchain_withQ_conv2bit_linear2bit/CNN_mnist_withQ_iidFalse_2num_bit_16num_users_0.005lr_200epoch0to199", 'rb'))
acc_withQ_2bit = data_withQ_2bit[0]
loss_withQ_2bit = data_withQ_2bit[1]
time_withQ_2bit = data_withQ_2bit[2]
time_mean_withQ_2bit = mean(time_withQ_2bit)


data_withQ_4bit = pickle.load(open(
    "log_mnist_without_blockchain_withQ_conv4bit_linear4bit/CNN_mnist_withQ_iidFalse_4num_bit_16num_users_0.005lr_200epoch0to199", 'rb'))
acc_withQ_4bit = data_withQ_4bit[0]
loss_withQ_4bit = data_withQ_4bit[1]
time_withQ_4bit = data_withQ_4bit[2]
time_mean_withQ_4bit = mean(time_withQ_4bit)


# data_withS = pickle.load(open(
#     "log_mnist_without_blockchain_withS_conv0.9_linear0.9/CNN_mnist_withS_iidTrue_32num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
# acc_withS = data_withS[0]
# loss_withS = data_withS[1]
# time_withS = data_withS[2]
# time_mean_withS = mean(time_withS)
#
#
# data_withQS = pickle.load(open(
#     "log_mnist_without_blockchain_withQS_conv3bit_linear2bit_conv0.9_linear0.9/CNN_mnist_iidTrue_3num_bit_10num_users_0.005lr_100epoch", 'rb'))
# acc_withQS = data_withQS[0]
# loss_withQS = data_withQS[1]
# time_withQS = data_withQS[2]
# time_mean_withQS = mean(time_withQS)
#
#
# data1_withQS = pickle.load(open(
#     "log_mnist_without_blockchain_withQS_conv2bit_linear2bit_conv0.9_linear0.9/CNN_mnist_withQS_iidTrue_2num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
# acc1_withQS = data1_withQS[0]
# loss1_withQS = data1_withQS[1]
# time1_withQS = data1_withQS[2]
# time1_mean_withQS = mean(time1_withQS)

plt.figure()
# plt.subplot(221)
plt.title('Average accuracy vs Communication rounds')
plt.plot(range(len(acc_base_16num)), acc_base_16num, label='baseline_16num_user --1728.4KB', linewidth=1)
plt.plot(range(len(acc_base_20num)), acc_base_20num, label='baseline_20num_user --1728.4KB', linewidth=1)
plt.plot(range(len(acc_base_50num)), acc_base_50num, label='baseline_50num_user --1728.4KB', linewidth=1)
plt.plot(range(len(acc_base_100num)), acc_base_100num, label='baseline_100num_user --1728.4KB', linewidth=1)
plt.plot(range(len(acc_withQ_2bit)), acc_withQ_2bit, label='with_quantization_conv2bit_linear2bit_16num_users', linewidth=1)
plt.plot(range(len(acc_withQ)), acc_withQ, label='with_quantization_conv3bit_linear2bit_20num_users', linewidth=1)
plt.plot(range(len(acc_withQ_4bit)), acc_withQ_4bit, label='with_quantization_conv4bit_linear4bit_16num_users', linewidth=1)
# plt.plot(range(len(acc_withS)), acc_withS, label='with_topk_conv0.9_linear0.9 --175.144KB', linewidth=1)
# plt.plot(range(len(acc_withQS)), acc_withQS, label='Quantization_conv3bit_linear2bit Topk_conv0.9_linear0.9 --49.4KB', linewidth=1)
# plt.plot(range(len(acc1_withQS)), acc1_withQS, label='Quantization_conv2bit_linear2bit Topk_conv0.9_linear0.9 --13.9KB', linewidth=1, ls='-.')

# plt.axhline(98, 0, 100, color='gray', ls='--', linewidth=0.5)
plt.axhline(95, 0, 100, color='gray', ls='--', linewidth=0.5)
plt.axhline(97, 0, 100, color='gray', ls='--', linewidth=0.5)
plt.yticks([20, 30, 40, 50, 60, 70, 80, 90, 95, 97], ['20', '30', '40', '50', '60', '70', '80', '90', '95', '97'])

plt.ylabel('Test Accuracy')
plt.legend()

#
# plt.subplot(222)
# plt.title('Training Loss vs Communication rounds')
# plt.plot(range(len(loss_base)), loss_base, label='baseline --1728.4KB', linewidth=1)
# plt.plot(range(len(loss_withQ)), loss_withQ, label='with_quantization_conv3bit_linear2bit --113.8KB', linewidth=1)
# plt.plot(range(len(loss_withS)), loss_withS, label='with_topk_conv0.9_linear0.9 --712.1KB', linewidth=1)
# plt.plot(range(len(loss_withQS)), loss_withQS, label='Quantization_conv3bit_linear2bit Topk_conv0.9_linear0.9 --49.4KB', linewidth=1)
#
# plt.ylabel('Test Loss')
# plt.legend()
#
#
# plt.subplot(223)
# plt.title('Running time vs Communication rounds')
# plt.plot(range(len(time_base)), time_base, label='baseline --1728.4KB', linewidth=1)
# plt.plot(range(len(time_withQ)), time_withQ, label='with_quantization_conv3bit_linear2bit --113.8KB', linewidth=1)
# plt.plot(range(len(time_withS)), time_withS, label='with_topk_conv0.9_linear0.9 --712.1KB', linewidth=1)
# plt.plot(range(len(time_withQS)), time_withQS, label='Quantization_conv3bit_linear2bit Topk_conv0.9_linear0.9 --49.4KB', linewidth=1)
#
# plt.ylabel('Running Time')
# plt.legend()


plt.show()