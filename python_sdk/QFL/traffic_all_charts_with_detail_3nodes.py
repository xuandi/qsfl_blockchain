import pickle
import matplotlib.pyplot as plt
from numpy import *
import numpy as np

data_base = pickle.load(open(
    "log_traffic_without_blockchain_withoutQS/LeNet_traffic_iidTrue_32num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc_base = data_base[0]
loss_base = data_base[1]
time_base = data_base[2]
time_mean_base = mean(time_base)


data1_base_blockchain = pickle.load(open(
    "log_traffic_with_blockchain_optimize_fisco_client/LeNet_traffic_withoutQS_iidTrue_32num_bit_10num_users_0.005lr_100epoch0to99_3nodes", 'rb'))
acc_base_blockchain = data1_base_blockchain[0]
loss_base_blockchain = data1_base_blockchain[1]
time_base_blockchain = data1_base_blockchain[2]
time_mean_base_blockchain = mean(time_base_blockchain)


data_withQ = pickle.load(open(
    "log_traffic_without_blockchain_withQ_conv4bit_linear6bit/LeNet_traffic_iidTrue_4num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc_withQ = data_withQ[0]
loss_withQ = data_withQ[1]
time_withQ = data_withQ[2]
time_mean_withQ = mean(time_withQ)


data_withQ = pickle.load(open(
    "log_traffic_with_blockchain_optimize_fisco_client/LeNet_traffic_withQ_iidTrue_4num_bit_10num_users_0.005lr_100epoch0to99_3nodes", 'rb'))
acc_withQ_blockchain = data_withQ[0]
loss_withQ_blockchain = data_withQ[1]
time_withQ_blockchain = data_withQ[2][1:]
time_mean_base_blockchain = mean(time_withQ_blockchain)


data_withS = pickle.load(open(
    "log_traffic_without_blockchain_withS_conv0.6_linear0.8/LeNet_traffic_iidTrue_32num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc_withS = data_withS[0]
loss_withS = data_withS[1]
time_withS = data_withS[2]
time_mean_withS = mean(time_withS)

data_withS_blockchain = pickle.load(open(
    "log_traffic_with_blockchain_optimize_fisco_client/LeNet_traffic_withS_iidTrue_32num_bit_10num_users_0.005lr_100epoch0to99_3nodes", 'rb'))
acc_withS_blockchain = data_withS_blockchain[0]
loss_withS_blockchain = data_withS_blockchain[1]
time_withS_blockchain = data_withS_blockchain[2][1:]
time_mean_withS_blockchain = mean(time_withS_blockchain)


data_withQS = pickle.load(open(
    "log_traffic_without_blockchain_withQS_conv4bit_linear6bit_conv0.6_linear0.8/LeNet_traffic_iidTrue_6num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc_withQS = data_withQS[0]
loss_withQS = data_withQS[1]
time_withQS = data_withQS[2]
time_mean_withQS = mean(time_withQS)


data_withQS_blockchain = pickle.load(open(
    "log_traffic_with_blockchain_optimize_fisco_client/LeNet_traffic_withQS_iidTrue_4num_bit_10num_users_0.005lr_100epoch0to99_3nodes", 'rb'))
acc_withQS_blockchain = data_withQS_blockchain[0]
loss_withQS_blockchain = data_withQS_blockchain[1]
time_withQS_blockchain = data_withQS_blockchain[2]
time_mean_withQS_blockchain = mean(time_withQS_blockchain)



plt.figure()
plt.subplot(221)
plt.title('Test accuracy vs Communication rounds')
plt.plot(range(len(acc_base)), acc_base, label='Baseline --1099.44KB', linewidth=1)
plt.plot(range(len(acc_base_blockchain)), acc_base_blockchain, label='Baseline_with_blockchain --1099.44KB', linewidth=1.5, ls='-.')
plt.plot(range(len(acc_withQ)), acc_withQ, label='Quan_conv4bit_linear6bit --195.021KB', linewidth=1)
plt.plot(range(len(acc_withQ_blockchain)), acc_withQ_blockchain, label='Quan_conv4bit_linear6bit_with_blockchain --195.021KB', linewidth=1.5, ls='-.')
plt.plot(range(len(acc_withS)), acc_withS, label='TopK_conv0.6_linear0.8 --268.8KB', linewidth=1)
plt.plot(range(len(acc_withS_blockchain)), acc_withS_blockchain, label='TopK_conv0.6_linear0.8_with_blockchain --268.8KB', linewidth=1.5, ls='-.')
plt.plot(range(len(acc_withQS)), acc_withQS, label='Quan_conv4bit_linear6bit TopK_conv0.6_linear0.8 --47.3KB', linewidth=1.5)
plt.plot(range(len(acc_withQS_blockchain)), acc_withQS_blockchain, label='Quan_conv4bit_linear6bit TopK_conv0.6_linear0.8_with_blockchain --47.3KB', linewidth=1.5, ls='-.')
plt.axhline(93, 0, 100, color='gray', ls='--', linewidth=0.5)
plt.yticks([20, 30, 40, 50, 60, 70, 80, 90, 93], ['20', '30', '40', '50', '60', '70', '80', '90', '93'])
plt.ylabel('Test Accuracy')
plt.legend()


plt.subplot(222)
plt.title('Test Loss vs Communication rounds')
plt.plot(range(len(loss_base)), loss_base, label='Baseline --1099.44KB', linewidth=1)
plt.plot(range(len(loss_base_blockchain)), loss_base_blockchain, label='Baseline_with_blockchain --1099.44KB', linewidth=1.5, ls='-.')
plt.plot(range(len(loss_withQ)), loss_withQ, label='Quan_conv4bit_linear6bit --195.021KB', linewidth=1)
plt.plot(range(len(loss_withQ_blockchain)), loss_withQ_blockchain, label='Quan_conv4bit_linear6bit_with_blockchain --195.021KB', linewidth=1.5, ls='-.')
plt.plot(range(len(loss_withS)), loss_withS, label='TopK_conv0.6_linear0.8 --268.8KB', linewidth=1)
plt.plot(range(len(loss_withS_blockchain)), loss_withS_blockchain, label='TopK_conv0.6_linear0.8_with_blockchain --268.8KB', linewidth=1.5, ls='-.')
plt.plot(range(len(loss_withQS)), loss_withQS, label='Quan_conv4bit_linear6bit TopK_conv0.6_linear0.8 --47.3KB', linewidth=1.5)
plt.plot(range(len(loss_withQS_blockchain)), loss_withQS_blockchain, label='Quan_conv4bit_linear6bit TopK_conv0.6_linear0.8_with_blockchain --47.3KB', linewidth=1.5, ls='-.')
plt.ylabel('Test Loss')
plt.legend()

plt.subplot(223)
plt.title('Running time vs Communication rounds')
p1, = plt.plot(range(len(time_base)), time_base, label='Baseline --1099.44KB'.format(time_mean_base), linewidth=1)
p2, = plt.plot(range(len(time_base_blockchain)), time_base_blockchain, label='Baseline_with_blockchain --1099.44KB'.format(time_mean_base_blockchain), linewidth=1.5, ls='-.')
p3, = plt.plot(range(len(time_withQ)), time_withQ, label='Quan_conv4bit_linear6bit --195.021KB', linewidth=1)
p4, = plt.plot(range(len(time_withQ_blockchain)), time_withQ_blockchain, label='Quan_conv4bit_linear6bit_with_blockchain --195.021KB', linewidth=1.5, ls='-.')
p5, = plt.plot(range(len(time_withS)), time_withS, label='TopK_conv0.6_linear0.8 --268.8KB', linewidth=1)
p6, = plt.plot(range(len(time_withS_blockchain)), time_withS_blockchain, label='TopK_conv0.6_linear0.8_with_blockchain --268.8KB', linewidth=1.5, ls='-.')
p7, = plt.plot(range(len(time_withQS)), time_withQS, label='Quan_conv4bit_linear6bit TopK_conv0.6_linear0.8 --47.3KB', linewidth=1.5)
p8, = plt.plot(range(len(time_withQS_blockchain)), time_withQS_blockchain, label='Quan_conv4bit_linear6bit TopK_conv0.6_linear0.8_with_blockchain --47.3KB', linewidth=1.5, ls='-.')
plt.ylabel('Running Time(Seconds)')
plt.legend(loc='lower center', bbox_to_anchor=(0.5, 0.1))

# only trading transaction:
only_trade_chain_1000transactions = round(2576 * 4 / 4 /1024, 2)
only_trade_chain_5000transactions = round((11152 + 10624 + 11136 + 10632) / 4 /1024, 2)

# Base line, without quantization and topK, each task train 100 or 40 rounds.
baseline_1Task = round((2100224 + 2152944 + 2140176 + 2119168) / 4 /1024, 2)
baseline_2Task = round((4328288 + 4221280 + 4231504 + 4179296) / 4 /1024, 2)
baseline_1Task_40Rounds = round((918856 + 936768 + 916784 + 936752) / 4 /1024, 2)
baseline_2Task_40Rounds = round((1861088 + 1886176 + 1777632 + 1883616) / 4 /1024, 2)
baseline_2Task_with_2000Trans = round(((4328288 + 4221280 + 4231504 + 4179296) / 4 /1024 + 2 * only_trade_chain_1000transactions), 2)


# only with quantization, each task train 100 or 40 rounds: conv=4 bit; linear=6 bit; grad_size=195.021 KB
only_Quand_1Task = round((369672 + 383512 + 387072 + 386568) / 4 /1024, 2)
only_Quand_2Task = round((767296 + 756032 + 761648 + 761664) / 4 /1024, 2)
only_Quand_1Task_40Rounds = round((159024 + 160576 + 163120 + 162112) /4 /1024, 2)


# only with topK, each task train 100 or 40 rounds: conv=0.6(select 40% parameters); linear=0.8 (select 20% parameters); grad_size=268.8 KB
only_Topk_1Task = round((726008 + 738312 + 732672 + 755704) / 4 /1024, 2)
only_Topk_2Task = round((1480032 + 1454928 + 1436464 + 1475920) / 4 /1024, 2)
only_Topk_1Task_40Rounds = round((311600 + 314184 + 311104 + 317232) /4 /1024, 2)


# with quantization and topK, each task train 100 or 40 rounds: Quand: conv=4 bit; linear=6 bit; TopK:conv=0.6; linear=0.8; grad_size=47.3 KB
quand_and_Topk_1Task = round((283632 + 288264 + 284696 + 280064) / 4 /1024, 2)
quand_and_Topk_2Task = round((586144 + 564144 + 561568 + 585648) / 4 /1024, 2)
quand_and_Topk_1Task_40Rounds = round((119112 + 118080 + 120112 + 121144) / 4 /1024, 2)

quand_and_Topk_2Task_with_2000Trans = round(((586144 + 564144 + 561568 + 585648) / 4 /1024+ 2 * only_trade_chain_1000transactions), 2)


list = [only_trade_chain_5000transactions, only_trade_chain_5000transactions, only_Topk_1Task_40Rounds, only_Quand_1Task_40Rounds, quand_and_Topk_1Task_40Rounds]
list2 = [0, baseline_1Task_40Rounds, 0, 0, 0]
list2_3= [only_trade_chain_5000transactions, (only_trade_chain_5000transactions + baseline_1Task_40Rounds), only_Topk_1Task_40Rounds, only_Quand_1Task_40Rounds, quand_and_Topk_1Task_40Rounds]
list3 = [0, (baseline_2Task_40Rounds - baseline_1Task_40Rounds), 0, 0, 0]
plt.subplot(224)
plt.title('Storage cost compare')
labels = ['5000 Trans', 'baseline', 'with Top-k', 'with Quan', 'with Quan&Top-k']
index = np.arange(len(labels))
plt.bar(index, list, align='center')
plt.bar(index, list2, align='center', bottom=list)
plt.bar(index, list3, align='center', bottom=list2_3)
plt.xticks(index, labels)

for a, b in zip(index, list):
    plt.text(a, b + 0.05, str(b)+'MB', ha='center', va='bottom')
    if a == 1:
        plt.text(a, baseline_1Task_40Rounds * 0.5, 'task1\n('+str(round(baseline_1Task_40Rounds, 2))+')MB', ha='center', va='bottom', color='white')
        plt.text(a, (baseline_1Task_40Rounds + only_trade_chain_5000transactions) +0.05, str(round((baseline_1Task_40Rounds + only_trade_chain_5000transactions), 2))+'MB', ha='center', va='bottom')
        plt.text(a, baseline_2Task_40Rounds * 0.75, 'task2\n('+str(round((baseline_2Task_40Rounds - baseline_1Task_40Rounds),2))+')MB', ha='center', va='bottom', color='white')
        plt.text(a, (baseline_2Task_40Rounds + only_trade_chain_5000transactions +0.05), str(round((baseline_2Task_40Rounds + only_trade_chain_5000transactions), 2))+'MB', ha='center', va='bottom')

plt.ylabel('Storage cost (MB)')


plt.show()