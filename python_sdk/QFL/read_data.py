import pickle
import matplotlib.pyplot as plt
#
data_50R = pickle.load(open("log_without_blockchain/NetCNN_less_iidFalse_mnist_2num_bit_16num_users_0.01lr_50epoch", 'rb'))
data_100R= pickle.load(open("log_without_blockchain/NetCNN_less_iidFalse_mnist_2num_bit_16num_users_0.01lr_500epoch", 'rb'))


plt.figure()
plt.subplot(221)  #split to 2*2 sub figure, and put this sub figure in the first place.
plt.title('Average Accuracy vs Communication rounds')
plt.plot(range(len(data_50R[0])), data_50R[0], color='r')
plt.plot(range(len(data_100R[0][0:50])), data_100R[0][0:50], color='b', ls='-.')
plt.ylabel('Training accuracy')

plt.subplot(222) #split to 2*2 sub figure, and put this sub figure in the second place,(the third and forth are empty.
plt.title('Average Loss vs Communication rounds')
plt.plot(range(len(data_50R[1])), data_50R[1], color='r')
plt.plot(range(len(data_100R[1][0:100])), data_100R[1][0:100], color='b', ls='-.')
plt.ylabel('Training loss')


plt.subplot(223)  #split to 2*2 sub figure, and put this sub figure in the first place.
plt.title('Running time vs Communication rounds')
plt.plot(range(len(data_50R[2])), data_50R[2])
plt.plot(range(len(data_100R[2][0:100])), data_100R[2][0:100], color='b', ls='-.')
plt.ylabel('Running time')

plt.show()
# plt.savefig('./log/10_rouds_accuracy.png')
#
# plt.figure()
# plt.title('Training Loss vs Communication rounds')
# plt.plot(range(len(data[1])), data[1])
# plt.ylabel('train_loss')
# plt.savefig('./log/10_rouds_loss.png')
#
#
