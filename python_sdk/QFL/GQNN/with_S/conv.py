import torch
import torch.nn as nn
import numpy as np
import math
import copy
from bitarray import bitarray

class GQConv2d(nn.Conv2d):
    r"""Applies a `Gradient Quantization` 2D convolution over an input signal
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1,
                 bias=True, padding_mode='zeros'):
        super(GQConv2d, self).__init__(in_channels, out_channels, kernel_size, 
        stride=stride, padding=padding, dilation=dilation, groups=groups,
        bias=bias, padding_mode=padding_mode)
        

    @torch.no_grad()
    def quantize_grad(self, initial, num_bit=4, prune_rate=0.9):  # when it's on 'only topk" mode. the num_bit will set to be 32 in model_withS.py
        """
        This method can be ONLY called by the local model.
        initial: the initial layer distributed from the server at CURRENT global iteration.
                 NOT the layer distributed at the first global iteration!
        num_bit: how many bits are used to represent the quantized gradient  
        """
        
        grad = self.weight - initial.weight
        grad = grad.cpu()
        
        # ========== sparsify =================
        filter_norm = torch.norm(grad, p=2, dim=(1,2,3))
        
        kth = math.floor(prune_rate * self.out_channels) + 1
        threshold = filter_norm.view(-1).kthvalue(kth).values.item()
        
        mask = filter_norm >= threshold
        grad = grad[mask]
        mask = mask.cpu().numpy()
        bit_mask = bitarray(mask.tolist())
        # ======================================
        
        grad = grad.reshape(-1).numpy()
        grad_bias = self.bias - initial.bias
        grad_bias = grad_bias.cpu().reshape(-1).numpy()
        
        # ========== if num_bit is 32 ============
        if num_bit >= 32:
            ready_to_save = (bit_mask, grad, grad_bias)
            return ready_to_save
        # =======================================
            
            
        sgn = grad > 0

        value = np.abs(grad)
        max_val = value.max()
        
        levels = 2 ** (num_bit-1)
        discrete = np.linspace(0, max_val, levels+1)
        index_grad = [np.zeros_like(grad, dtype=np.bool) for _ in range(num_bit - 1)]

        for i, bit in enumerate(reversed(range(num_bit-1))):
            mask = value >= discrete[2**bit]
            index_grad[i][mask] = True
            value = value - mask * discrete[2**bit]
            
        bit_grad = [bitarray(bool_grad.tolist()) for bool_grad in index_grad]
        bit_sgn = bitarray(sgn.tolist())
        ready_to_save = (bit_mask, bit_sgn, bit_grad, max_val, grad_bias)

        return ready_to_save
    
    @torch.no_grad()
    def decode_grad(self, initial, ready_to_save):
        if len(ready_to_save) == 3:
            bit_mask, grad, grad_bias = ready_to_save
            mask = torch.tensor(bit_mask.tolist(), dtype=torch.bool, device=self.weight.device)
            
            shape = self.weight.data.shape
            non_zeros_shape = (int(torch.sum(mask).item()), shape[1], shape[2], shape[3])
            
            grad = torch.tensor(grad, device=self.weight.device).reshape(non_zeros_shape)
            grad_bias = torch.tensor(grad_bias, device=self.weight.device).reshape(self.out_channels)
            self.weight.data = copy.deepcopy(initial.weight.data)
            self.weight.data[mask] += grad
            self.bias.data = initial.bias.data + grad_bias
            return
            
        
        bit_mask, bit_sgn, bit_grad, max_val, grad_bias = ready_to_save
        mask = torch.tensor(bit_mask.tolist(), dtype=torch.bool, device=self.weight.device)
        
        shape = self.weight.data.shape
        non_zeros_shape = (int(torch.sum(mask).item()), shape[1], shape[2], shape[3])
        grad = torch.zeros(non_zeros_shape, device=self.weight.device)
        
        levels = 2 ** len(bit_grad)
        discrete = np.linspace(0, max_val, levels+1)
        
        for i, bt in enumerate(reversed(bit_grad)):
            grad += discrete[2**i] * torch.tensor(bt.tolist(), dtype=torch.bool, device=self.weight.device).reshape(non_zeros_shape)
        
        sgn = torch.tensor(bit_sgn.tolist(), dtype=torch.float, device=self.weight.device).reshape(non_zeros_shape)
        sgn = sgn * 2 - 1  # {0, 1} -> {-1, 1}
        
        if len(bit_grad) == 0:
            grad = sgn * max_val
        else:
            grad = sgn * grad
        
        grad_bias = torch.tensor(grad_bias, device=self.weight.device).reshape(self.out_channels)
        
        # w_{t} = w_{t-1} + G_{t}
        self.weight.data = copy.deepcopy(initial.weight.data)
        self.weight.data[mask] += grad
        self.bias.data = initial.bias.data + grad_bias