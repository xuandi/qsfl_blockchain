import torch
import torch.nn as nn
import numpy as np
import math
from bitarray import bitarray

class GQConv2d(nn.Conv2d):
    r"""Applies a `Gradient Quantization` 2D convolution over an input signal
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1,
                 bias=True, padding_mode='zeros'):
        super(GQConv2d, self).__init__(in_channels, out_channels, kernel_size, 
        stride=stride, padding=padding, dilation=dilation, groups=groups,
        bias=bias, padding_mode=padding_mode)
        

    @torch.no_grad()
    def quantize_grad(self, initial, num_bit=4):
        num_bit = 3  # traffic_idd_false=4bit; mnist_idd_true=2bit; mnist_idd_false=3bit
        """
        This method can be ONLY called by the local model.
        initial: the initial layer distributed from the server at CURRENT global iteration.
                 NOT the layer distributed at the first global iteration!
        num_bit: how many bits are used to represent the quantized gradient  
        """
        
        grad = self.weight - initial.weight
        grad = grad.cpu().reshape(-1).numpy()
        grad_bias = self.bias - initial.bias
        grad_bias = grad_bias.cpu().reshape(-1).numpy()
        
        sgn = grad > 0

        value = np.abs(grad)
        max_val = value.max()
        
        levels = 2 ** (num_bit-1)
        discrete = np.linspace(0, max_val, levels+1)
        index_grad = [np.zeros_like(grad, dtype=np.bool) for _ in range(num_bit - 1)]

        for i, bit in enumerate(reversed(range(num_bit-1))):
            mask = value >= discrete[2**bit]
            index_grad[i][mask] = True
            value = value - mask * discrete[2**bit]
            
        bit_grad = [bitarray(bool_grad.tolist()) for bool_grad in index_grad]
        bit_sgn = bitarray(sgn.tolist())
        ready_to_save = (bit_sgn, bit_grad, max_val, grad_bias)

        return ready_to_save
    
    @torch.no_grad()
    def decode_grad(self, initial, ready_to_save):
        bit_sgn, bit_grad, max_val, grad_bias = ready_to_save
        grad = torch.zeros_like(self.weight.data)
        shape = self.weight.data.shape
        levels = 2 ** len(bit_grad)
        discrete = np.linspace(0, max_val, levels+1)
        
        for i, bt in enumerate(reversed(bit_grad)):
            grad += discrete[2**i] * torch.tensor(bt.tolist(), dtype=torch.bool, device=self.weight.device).reshape(shape)
        
        sgn = torch.tensor(bit_sgn.tolist(), dtype=torch.float, device=self.weight.device).reshape(shape)
        sgn = sgn * 2 - 1  # {0, 1} -> {-1, 1}
        
        if len(bit_grad) == 0:
            grad = sgn * max_val
        else:
            grad = sgn * grad
        
        grad_bias = torch.tensor(grad_bias, device=self.weight.device).reshape(shape[0])
        
        # w_{t} = w_{t-1} + G_{t}
        self.weight.data = initial.weight.data + grad
        self.bias.data = initial.bias.data + grad_bias