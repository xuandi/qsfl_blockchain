import torch
import torch.nn as nn


class GQLinear(nn.Linear):
    r"""Applies a `Communication-Aware` Linear operation over an input signal
    """

    def __init__(self, in_features, out_features, bias=True):

        super(GQLinear, self).__init__(in_features, out_features, bias=bias)

    @torch.no_grad()
    def quantize_grad(self, initial, num_bit=4):
        # num_bit = 2
        """
        This method can be ONLY called by the local model.
        initial: the initial layer distributed from the server at CURRENT global iteration.
                 NOT the layer distributed at the first global iteration!
        num_bit: how many bits are used to represent the quantized gradient
        """

        grad = self.weight - initial.weight
        grad = grad.cpu().reshape(-1).numpy()
        grad_bias = self.bias - initial.bias
        grad_bias = grad_bias.cpu().reshape(-1).numpy()
        ready_to_save = (grad, grad_bias)

        return ready_to_save

    @torch.no_grad()
    def decode_grad(self, initial, ready_to_save):
        grad, grad_bias = ready_to_save
        shape = self.weight.data.shape
        grad = torch.tensor(grad, device=self.weight.device).reshape(shape)
        grad_bias = torch.tensor(grad_bias, device=self.weight.device).reshape(shape[0])

        # w_{t} = w_{t-1} + G_{t}
        self.weight.data = initial.weight.data + grad
        self.bias.data = initial.bias.data + grad_bias