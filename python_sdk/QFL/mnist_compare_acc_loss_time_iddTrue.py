import pickle
import matplotlib.pyplot as plt
from numpy import *

data_base = pickle.load(open(
    "log_mnist_without_blockchain_withoutQS/CNN_mnist_withoutQS_iidTrue_2num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc_base = data_base[0]
loss_base = data_base[1]
time_base = data_base[2]
time_mean_base = mean(time_base)


data_withQ = pickle.load(open(
    "log_mnist_without_blockchain_withQ_conv3bit_linear2bit/CNN_mnist_iidTrue_2num_bit_10num_users_0.01lr_100epoch", 'rb'))
acc_withQ = data_withQ[0]
loss_withQ = data_withQ[1]
time_withQ = data_withQ[2]
time_mean_withQ = mean(time_withQ)

data1_withQ = pickle.load(open(
    "log_mnist_without_blockchain_withQ_conv2bit_linear2bit/CNN_mnist_withQ_iidTrue_2num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc1_withQ = data1_withQ[0]
loss1_withQ = data1_withQ[1]
time1_withQ = data1_withQ[2]
time1_mean_withQ = mean(time1_withQ)


data_withS = pickle.load(open(
    "log_mnist_without_blockchain_withS_conv0.9_linear0.9/CNN_mnist_withS_iidTrue_32num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc_withS = data_withS[0]
loss_withS = data_withS[1]
time_withS = data_withS[2]
time_mean_withS = mean(time_withS)


data_withQS = pickle.load(open(
    "log_mnist_without_blockchain_withQS_conv3bit_linear2bit_conv0.9_linear0.9/CNN_mnist_iidTrue_3num_bit_10num_users_0.005lr_100epoch", 'rb'))
acc_withQS = data_withQS[0]
loss_withQS = data_withQS[1]
time_withQS = data_withQS[2]
time_mean_withQS = mean(time_withQS)


data1_withQS = pickle.load(open(
    "log_mnist_without_blockchain_withQS_conv2bit_linear2bit_conv0.9_linear0.9/CNN_mnist_withQS_iidTrue_2num_bit_10num_users_0.005lr_100epoch0to99", 'rb'))
acc1_withQS = data1_withQS[0]
loss1_withQS = data1_withQS[1]
time1_withQS = data1_withQS[2]
time1_mean_withQS = mean(time1_withQS)

plt.figure()
plt.subplot(221)
plt.title('Average accuracy vs Communication rounds')
plt.plot(range(len(acc_base)), acc_base, label='baseline --1728.4KB', linewidth=1)
plt.plot(range(len(acc_withQ)), acc_withQ, label='with_quantization_conv3bit_linear2bit --113.8KB', linewidth=1)
plt.plot(range(len(acc1_withQ)), acc1_withQ, label='with_quantization_conv2bit_linear2bit --110.571KB', linewidth=1, ls='-.')
plt.plot(range(len(acc_withS)), acc_withS, label='with_topk_conv0.9_linear0.9 --175.144KB', linewidth=1)
plt.plot(range(len(acc_withQS)), acc_withQS, label='Quantization_conv3bit_linear2bit Topk_conv0.9_linear0.9 --49.4KB', linewidth=1)
plt.plot(range(len(acc1_withQS)), acc1_withQS, label='Quantization_conv2bit_linear2bit Topk_conv0.9_linear0.9 --13.9KB', linewidth=1, ls='-.')

# plt.axhline(98, 0, 100, color='gray', ls='--', linewidth=0.5)
plt.axhline(99, 0, 100, color='gray', ls='--', linewidth=0.5)
plt.axhline(98, 0, 100, color='gray', ls='--', linewidth=0.5)
plt.yticks([20, 30, 40, 50, 60, 70, 80, 90, 98, 99], ['20', '30', '40', '50', '60', '70', '80', '90', '98', '99'])

plt.ylabel('Test Accuracy')
plt.legend()


plt.subplot(222)
plt.title('Training Loss vs Communication rounds')
plt.plot(range(len(loss_base)), loss_base, label='baseline --1728.4KB', linewidth=1)
plt.plot(range(len(loss_withQ)), loss_withQ, label='with_quantization_conv3bit_linear2bit --113.8KB', linewidth=1)
plt.plot(range(len(loss_withS)), loss_withS, label='with_topk_conv0.9_linear0.9 --712.1KB', linewidth=1)
plt.plot(range(len(loss_withQS)), loss_withQS, label='Quantization_conv3bit_linear2bit Topk_conv0.9_linear0.9 --49.4KB', linewidth=1)

plt.ylabel('Test Loss')
plt.legend()


plt.subplot(223)
plt.title('Running time vs Communication rounds')
plt.plot(range(len(time_base)), time_base, label='baseline --1728.4KB', linewidth=1)
plt.plot(range(len(time_withQ)), time_withQ, label='with_quantization_conv3bit_linear2bit --113.8KB', linewidth=1)
plt.plot(range(len(time_withS)), time_withS, label='with_topk_conv0.9_linear0.9 --712.1KB', linewidth=1)
plt.plot(range(len(time_withQS)), time_withQS, label='Quantization_conv3bit_linear2bit Topk_conv0.9_linear0.9 --49.4KB', linewidth=1)

plt.ylabel('Running Time')
plt.legend()


plt.show()