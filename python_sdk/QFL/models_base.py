""" Models for MNIST
"""
import os
import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
from python_sdk.QFL.GQNN.without_QS import GQConv2d, GQLinear   # only save the original gradients, without quantization and sparsity

"""
Model 1: 2NN
A simple multilayer-perceptron with 2-hidden
layers with 200 units each using ReLu activations (199,210
total parameters), which we refer to as the MNIST 2NN.

Reference: Communication-Efficient Learning of Deep Networks from Decentralized Data
https://arxiv.org/pdf/1602.05629.pdf
"""
class Net2NN(nn.Module):
    def __init__(self):
        super(Net2NN, self).__init__()
        self.fc1 = GQLinear(28*28, 200)
        self.fc2 = GQLinear(200, 200)
        self.fc3 = GQLinear(200, 10)
        
        self.all_nn = [self.fc1, self.fc2, self.fc3]
        self.agg_data = 0 # how many data samples have been aggregated in the global model

    def forward(self, x):
        x = x.view(-1, 28*28) # flatten
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
    
    @torch.no_grad()
    def quantize_grad(self, initial, num_bit=4, fn='grad'):
        if not os.path.exists('./QFL/grad_base'):
            os.mkdir('./QFL/grad_base')
        
        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)
        pickle.dump(gradient, open('./QFL/grad_base/' + fn, 'wb'))
        return

    def decode_grad(self, initial, fn='grad'):
        gradient = pickle.load(open('./QFL/grad_base/' + fn, 'rb'))
        for nn, grad_layer in zip(initial.all_nn, gradient):
            self.decode_grad(nn, grad_layer)
            
    @torch.no_grad()
    def aggregate(self, oth, data):
        """Aggregation. Note that this function can be only called by the parameter server.
        
        Param:
        "oth" : local model uploaded by the client
        "data": the training data sample size of the client 
        """
        new_agg_data = self.agg_data + data
        agg_factor = self.agg_data / new_agg_data
        oth_factor = data / new_agg_data
        
        # loop over the models, and apply weighted sum to two model
        for agg_m, oth_m in zip(self.modules(), oth.modules()):
            if isinstance(agg_m, (nn.Conv2d, nn.Linear)):
                oth_m.weight.mul_(oth_factor)
                oth_m.bias.mul_(oth_factor)
                agg_m.weight.mul_(agg_factor)
                agg_m.bias.mul_(agg_factor)
                
                agg_m.weight.add_(oth_m.weight)
                agg_m.bias.add_(oth_m.bias)
        
        self.agg_data = new_agg_data
    
    def zero_agg_data(self):
        """Zero out the self.agg_data.
        This is called at the end/begining of each communication round.
        """
        self.agg_data = 0



""" 
Model 2: CNN
A CNN with two 5x5 convolution layers (the first with
32 channels, the second with 64, each followed with 2x2
max pooling), a fully connected layer with 512 units and
ReLu activation, and a final softmax output layer (1,663,370
total parameters).

Reference: Communication-Efficient Learning of Deep Networks from Decentralized Data
https://arxiv.org/pdf/1602.05629.pdf
"""
class NetCNN(nn.Module):
    def __init__(self):
        super(NetCNN, self).__init__()
        self.conv1 = GQConv2d(1, 32, 5, 1, padding=2)
        self.conv2 = GQConv2d(32, 64, 5, 1, padding=2)
        self.fc1 = GQLinear(7 * 7 * 64, 512)
        self.fc2 = GQLinear(512, 10)
        self.all_nn = [self.conv1, self.conv2, self.fc1, self.fc2]
        self.agg_data = 0 # how many data samples have been aggregated in the global model

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 7 * 7 * 64)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x

    @torch.no_grad()
    def quantize_grad_without_blockchain(self, initial, num_bit=4, fn='grad'):
        if not os.path.exists('./QFL/grad_base'):
            os.mkdir('./QFL/grad_base')
        
        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)

        pickle.dump(gradient, open('./QFL/grad_base/' + fn, 'wb'))
        return
    
    def decode_grad_without_blockchain(self, initial, fn='grad'):
        gradient = pickle.load(open('./QFL/grad_base/' + fn, 'rb'))
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)

    # blockchain
    def quantize_grad_with_blockchain(self, initial, num_bit=4, fn='grad'):
        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)
        return gradient

    # blockchain
    def decode_grad_with_blockchain(self, initial, gradient, fn='grad'):
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)

    @torch.no_grad()
    def aggregate(self, oth, data):
        """Aggregation. Note that this function can be only called by the parameter server.
        
        Param:
        "oth" : local model uploaded by the client
        "data": the training data sample size of the client 
        """
        new_agg_data = self.agg_data + data
        agg_factor = self.agg_data / new_agg_data
        oth_factor = data / new_agg_data
        
        # loop over the models, and apply weighted sum to two model
        for agg_m, oth_m in zip(self.modules(), oth.modules()):
            if isinstance(agg_m, (nn.Conv2d, nn.Linear)):
                oth_m.weight.mul_(oth_factor)
                oth_m.bias.mul_(oth_factor)
                agg_m.weight.mul_(agg_factor)
                agg_m.bias.mul_(agg_factor)
                
                agg_m.weight.add_(oth_m.weight)
                agg_m.bias.add_(oth_m.bias)
        
        self.agg_data = new_agg_data
    
    def zero_agg_data(self):
        """Zero out the self.agg_data.
        This is called at the end/begining of each communication round.
        """
        self.agg_data = 0


class CNN_mnist_base(nn.Module):
    def __init__(self):
        super(CNN_mnist_base, self).__init__()
        self.conv1 = GQConv2d(1, 20, 5, 1)
        self.conv2 = GQConv2d(20, 50, 5, 1)
        self.fc1 = GQLinear(4 * 4 * 50, 500)
        self.fc2 = GQLinear(500, 10)
        self.all_nn = [self.conv1, self.conv2, self.fc1, self.fc2]
        self.agg_data = 0  # how many data samples have been aggregated in the global model

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4 * 4 * 50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x

    @torch.no_grad()
    # without blockchain
    def quantize_grad_without_blockchain(self, initial, num_bit=4, fn='grad'):
        if not os.path.exists('./QFL/grad_base'):
            os.mkdir('./QFL/grad_base')

        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)

        pickle.dump(gradient, open('./QFL/grad_base/' + fn, 'wb'))
        return

    # without blockchain
    def decode_grad_without_blockchain(self, initial, fn='grad'):
        gradient = pickle.load(open('./QFL/grad_base/' + fn, 'rb'))
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)

    # blockchain
    def quantize_grad_with_blockchain(self, initial, num_bit=4, fn='grad'):
        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)
        return gradient

    # blockchain
    def decode_grad_with_blockchain(self, initial, gradient, fn='grad'):
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)

    @torch.no_grad()
    def aggregate(self, oth, data):
        """Aggregation. Note that this function can be only called by the parameter server.

        Param:
        "oth" : local model uploaded by the client
        "data": the training data sample size of the client
        """
        new_agg_data = self.agg_data + data
        agg_factor = self.agg_data / new_agg_data
        oth_factor = data / new_agg_data

        # loop over the models, and apply weighted sum to two model
        for agg_m, oth_m in zip(self.modules(), oth.modules()):
            if isinstance(agg_m, (nn.Conv2d, nn.Linear)):
                oth_m.weight.mul_(oth_factor)
                oth_m.bias.mul_(oth_factor)
                agg_m.weight.mul_(agg_factor)
                agg_m.bias.mul_(agg_factor)

                agg_m.weight.add_(oth_m.weight)
                agg_m.bias.add_(oth_m.bias)

        self.agg_data = new_agg_data

    def zero_agg_data(self):
        """Zero out the self.agg_data.
        This is called at the end/begining of each communication round.
        """
        self.agg_data = 0


class CNN_cifar_base(nn.Module):
    def __init__(self):
        super(CNN_cifar_base, self).__init__()
        self.conv1 = GQConv2d(3, 6, 5)
        self.conv2 = GQConv2d(6, 16, 5)
        self.fc1 = GQLinear(16 * 5 * 5, 120)
        self.fc2 = GQLinear(120, 84)
        self.fc3 = GQLinear(84, 10)
        self.all_nn = [self.conv1, self.conv2, self.fc1, self.fc2, self.fc3]
        self.agg_data = 0

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = F.max_pool2d(x, 2, 2)
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    @torch.no_grad()
    # without blockchain
    def quantize_grad_without_blockchain(self, initial, num_bit=4, fn='grad'):
        if not os.path.exists('./QFL/grad_base'):
            os.mkdir('./QFL/grad_base')

        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)

        pickle.dump(gradient, open('./QFL/grad_base/' + fn, 'wb'))
        return

    # without blockchain
    def decode_grad_without_blockchain(self, initial, fn='grad'):
        gradient = pickle.load(open('./QFL/grad_base/' + fn, 'rb'))
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)

    # blockchain
    def quantize_grad_with_blockchain(self, initial, num_bit=4, fn='grad'):
        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)
        return gradient

    # blockchain
    def decode_grad_with_blockchain(self, initial, gradient, fn='grad'):
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)


    @torch.no_grad()
    def aggregate(self, oth, data):
        """Aggregation. Note that this function can be only called by the parameter server.

        Param:
        "oth" : local model uploaded by the client
        "data": the training data sample size of the client
        """
        new_agg_data = self.agg_data + data
        agg_factor = self.agg_data / new_agg_data
        oth_factor = data / new_agg_data

        # loop over the models, and apply weighted sum to two model
        for agg_m, oth_m in zip(self.modules(), oth.modules()):
            if isinstance(agg_m, (nn.Conv2d, nn.Linear)):
                oth_m.weight.mul_(oth_factor)
                oth_m.bias.mul_(oth_factor)
                agg_m.weight.mul_(agg_factor)
                agg_m.bias.mul_(agg_factor)

                agg_m.weight.add_(oth_m.weight)
                agg_m.bias.add_(oth_m.bias)

        self.agg_data = new_agg_data

    def zero_agg_data(self):
        """Zero out the self.agg_data.
        This is called at the end/begining of each communication round.
        """
        self.agg_data = 0


class LeNet_traffic_base(nn.Module):
    '''
    Build LeNet Model
    '''

    def __init__(self):
        super(LeNet_traffic_base, self).__init__()

        self.conv1 = GQConv2d(3, 32, 5, 1, 2)
        self.conv2 = GQConv2d(32, 64, 5)
        self.fc1 = GQLinear(64 * 5 * 5, 128)
        self.fc2 = GQLinear(128, 84)
        self.fc3 = GQLinear(84, 62)
        self.all_nn = [self.conv1, self.conv2, self.fc1, self.fc2, self.fc3]
        self.agg_data = 0

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)

        x = x.view(x.size()[0], -1)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    @torch.no_grad()
    # without blockchain
    def quantize_grad_without_blockchain(self, initial, num_bit=4, fn='grad'):
        if not os.path.exists('./QFL/grad_base'):
            os.mkdir('./QFL/grad_base')

        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)

        pickle.dump(gradient, open('./QFL/grad_base/' + fn, 'wb'))
        return

    # without blockchain
    def decode_grad_without_blockchain(self, initial, fn='grad'):
        gradient = pickle.load(open('./QFL/grad_base/' + fn, 'rb'))
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)

    # blockchain
    def quantize_grad_with_blockchain(self, initial, num_bit=4, fn='grad'):
        gradient = []
        for nn, oth_nn in zip(self.all_nn, initial.all_nn):
            grad_layer = nn.quantize_grad(oth_nn, num_bit)
            gradient.append(grad_layer)
        return gradient

    # blockchain
    def decode_grad_with_blockchain(self, initial, gradient, fn='grad'):
        for nn, oth_nn, grad_layer in zip(self.all_nn, initial.all_nn, gradient):
            nn.decode_grad(oth_nn, grad_layer)

    @torch.no_grad()
    def aggregate(self, oth, data):
        """Aggregation. Note that this function can be only called by the parameter server.

        Param:
        "oth" : local model uploaded by the client
        "data": the training data sample size of the client
        """
        new_agg_data = self.agg_data + data
        agg_factor = self.agg_data / new_agg_data
        oth_factor = data / new_agg_data

        # loop over the models, and apply weighted sum to two model
        for agg_m, oth_m in zip(self.modules(), oth.modules()):
            if isinstance(agg_m, (nn.Conv2d, nn.Linear)):
                oth_m.weight.mul_(oth_factor)
                oth_m.bias.mul_(oth_factor)
                agg_m.weight.mul_(agg_factor)
                agg_m.bias.mul_(agg_factor)

                agg_m.weight.add_(oth_m.weight)
                agg_m.bias.add_(oth_m.bias)

        self.agg_data = new_agg_data

    def zero_agg_data(self):
        """Zero out the self.agg_data.
        This is called at the end/begining of each communication round.
        """
        self.agg_data = 0
