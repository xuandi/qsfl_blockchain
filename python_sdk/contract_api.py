#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
  FISCO BCOS/Python-SDK is a python client for FISCO BCOS2.0 (https://github.com/FISCO-BCOS/)
  FISCO BCOS/Python-SDK is free software: you can redistribute it and/or modify it under the
  terms of the MIT License as published by the Free Software Foundation. This project is
  distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Thanks for
  authors and contributors of eth-abi, eth-account, eth-hash，eth-keys, eth-typing, eth-utils,
  rlp, eth-rlp , hexbytes ... and relative projects
  @author: kentzhang
  @date: 2019-06
'''
from python_sdk.client.contractnote import ContractNote
import os
from python_sdk.eth_utils import to_checksum_address
from python_sdk.client.datatype_parser import DatatypeParser
from python_sdk.client.common.compiler import Compiler
from python_sdk.client.bcoserror import BcosException, BcosError
from python_sdk.client_config import client_config
from python_sdk.UTILITIES import curr_time
import traceback
import configparser


def get_contract_addr(contract_address_path, contract_name):
    config = configparser.ConfigParser()
    config.read(contract_address_path)
    contract_addr = to_checksum_address(config.get('address', contract_name))
    return contract_addr


def get_worker_name(contract_address_path, addr):
    config = configparser.ConfigParser()
    config.read(contract_address_path)
    for worker in config['accounts']:
        if config.get('accounts', worker) == addr:
            return worker


def get_account_addr(accounts_path, account_list):
    config = configparser.ConfigParser()
    config.read(accounts_path)
    address_list = list()
    for i in account_list:
        address = to_checksum_address(config.get('accounts', i))
        address_list.append(address)
    return address_list


def get_contract_abi(contract_path, contract_name):
    abi_file = contract_path + contract_name + ".abi"
    data_parser = DatatypeParser()
    data_parser.load_abi_file(abi_file)
    contract_abi = data_parser.contract_abi
    return contract_abi


def write_data_to_fisco_with_args(client, contract_path, contract_address_path, contract_name, function_name, transactionArgs):
    contract_abi = get_contract_abi(contract_path, contract_name)
    to_address = get_contract_addr(contract_address_path, contract_name)
    receipt = {}
    try:
        # client = BcosClient() # must be commanded, otherwise will case the running time increase linearly.
        # print("\n "+ curr_time() + "<< Writing data to FISCO BCOS via the {} function of {} smartContract >>".format(function_name, contract_name))
        receipt = client.sendRawTransactionGetReceipt(to_address, contract_abi, function_name, transactionArgs)
    except BcosException as e:
        print("execute demo_transaction failed ,BcosException for: {}".format(e))
        traceback.print_exc()
    except BcosError as e:
        print("execute demo_transaction failed ,BcosError for: {}".format(e))
        traceback.print_exc()
    except Exception as e:
        client.finish()
        traceback.print_exc()
    return receipt
    # client.finish() # must be commanded, otherwise will case the running time increase linearly.

def get_data_from_fisco_without_args(client, contract_path, contract_address_path, contract_name, function_name):
    contract_abi = get_contract_abi(contract_path, contract_name)
    to_address = get_contract_addr(contract_address_path, contract_name)
    try:
        # client = BcosClient() # must be commanded, otherwise will case the running time increase linearly.
        # 调用一下call，获取数据
        # print(curr_time() + " << Calling data from FISCO BCOS via the {} function of {} smartContract >>".format(function_name, contract_name))
        res = client.call(to_address, contract_abi, function_name)
        return res
    except BcosException as e:
        print("execute demo_transaction failed ,BcosException for: {}".format(e))
        traceback.print_exc()
    except BcosError as e:
        print("execute demo_transaction failed ,BcosError for: {}".format(e))
        traceback.print_exc()
    except Exception as e:
        client.finish()
        traceback.print_exc()
    # client.finish() # must be commanded, otherwise will case the running time increase linearly.


def get_data_from_fisco_with_args(client, contract_path, contract_address_path, contract_name, function_name, args):
    contract_abi = get_contract_abi(contract_path, contract_name)
    to_address = get_contract_addr(contract_address_path, contract_name)
    try:
        # client = BcosClient() # must be commanded, otherwise will case the running time increase linearly.
        # 调用一下call，获取数据
        # print(curr_time() + " << Calling data from FISCO BCOS via the {} function of {} smartContract >>".format(function_name, contract_name))
        res = client.call(to_address, contract_abi, function_name, args)
        return res
    except BcosException as e:
        print("execute demo_transaction failed ,BcosException for: {}".format(e))
        traceback.print_exc()
    except BcosError as e:
        print("execute demo_transaction failed ,BcosError for: {}".format(e))
        traceback.print_exc()
    except Exception as e:
        client.finish()
        traceback.print_exc()
    # client.finish() # must be commanded, otherwise will case the running time increase linearly.

class SmartContract:
    def __init__(self, contract_name):
        self.contract_path = "./contracts/"
        self.contract_address_path = "./bin/contract.ini"
        self.contract_name = contract_name

    def get_contract_address_path(self):
        return self.contract_address_path

    def get_contract_path(self):
        return self.contract_path

    def deploy_contract(self, client):
        target_path = self.contract_path + self.contract_name

        if os.path.isfile(client_config.solc_path) or os.path.isfile(client_config.solcjs_path):
            Compiler.compile_file(target_path + ".sol")
        abi_file = target_path + ".bin"

        try:
            # client = BcosClient()
            # print(client.getinfo())
            # 部署合约
            print(curr_time() + " >>Deploying {} smart contract".format(self.contract_name))
            with open(target_path + ".bin", 'r') as load_f:
                contract_bin = load_f.read()
                load_f.close()
            result = client.deploy(contract_bin)
            contract_name = os.path.splitext(os.path.basename(abi_file))[0]
            # 把部署结果存入文件备查
            ContractNote.save_address_to_contract_note(contract_name, result["contractAddress"])

        except BcosException as e:
            print("execute demo_transaction failed ,BcosException for: {}".format(e))
            traceback.print_exc()
        except BcosError as e:
            print("execute demo_transaction failed ,BcosError for: {}".format(e))
            traceback.print_exc()
        except Exception as e:
            # client.finish()
            traceback.print_exc()
        return result["contractAddress"]

    def write_data(self, client, function_name, transactionArgs):  # transactionArgs is an array
        return write_data_to_fisco_with_args(client, self.contract_path, self.contract_address_path, self.contract_name,
                                             function_name, transactionArgs)

    def get_data_without_args(self, client, function_name):
        return get_data_from_fisco_without_args(client, self.contract_path, self.contract_address_path, self.contract_name,
                                                function_name)

    def get_data_with_args(self, client, function_name, args):  # args is an array
        return get_data_from_fisco_with_args(client, self.contract_path, self.contract_address_path, self.contract_name,
                                             function_name, args)  # reture a tuple, use args[0] to get the result.


class Task:
    def __init__(self):
        self.smart_contract = SmartContract('TaskMain')

    def get_curr_task_id(self, client):
        res = self.smart_contract.get_data_without_args(client, 'getCurrentTaskId')
        # return string
        return str(res[0])

    def create_new_task(self, client, training_epochs, batchSize, lr):
        workers_address_list = get_account_addr('./bin/accounts/accounts.ini', ['worker1', 'worker2', 'worker3'])  # now each task I only add three worker as the worker list, but this should be the real worker address list, the number of workers = num_users
        transactionArgs = [str(int(self.get_curr_task_id(client))+1), workers_address_list, training_epochs, batchSize, lr]
        result = self.smart_contract.write_data(client, 'addTask', transactionArgs)
        # return result
        return str(int(result['output'], 0))


class ModelStorage:
    def __init__(self):
        self.smart_contract = SmartContract('ModelStorageMain')
        self.accounts_path = './bin/accounts/accounts.ini'

    def get_accounts_path(self):
        return self.accounts_path

    def upload_model(self, client, epoch, modelStr):
        myAddr = to_checksum_address(0x54b849c83b55edb6b1a0dc0b0567860d3bb8a805)
        transactionArgs = [myAddr, epoch, modelStr]
        result = self.smart_contract.write_data(client, 'recordModel', transactionArgs)

    def get_model_by_epoch(self, client, curr_epoch):
        transactionArgs = [curr_epoch]
        res = self.smart_contract.get_data_with_args(client, 'getModelByEpoch', transactionArgs)
        model_str = ''.join(res[0])
        return model_str

    def get_curr_epoch(self, client):
        epoch_result= self.smart_contract.get_data_without_args(client, 'getCurrEpochNum')
        return str(epoch_result[0])

class GradStorage:
    def __init__(self):
        self.smart_contract = SmartContract('GradStorageMain')
        self.accounts_path = './bin/accounts/accounts.ini'

    def get_accounts_path(self):
        return self.accounts_path

    def upload_grad(self, client, epoch, gradStr):
        myAddr = to_checksum_address(0x54b849c83b55edb6b1a0dc0b0567860d3bb8a805)
        transactionArgs = [myAddr, epoch, gradStr]
        result = self.smart_contract.write_data(client, 'recordGradient', transactionArgs)

    def get_newest_grad(self, client, curr_epoch):
        transactionArgs = [curr_epoch]
        res = self.smart_contract.get_data_with_args(client, 'getNewestGradient', transactionArgs)
        gradStr = ''.join(res[0])
        return gradStr
