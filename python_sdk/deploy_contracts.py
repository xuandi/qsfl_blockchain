import sys
sys.path.append('..')
from python_sdk.contract_api import SmartContract, get_contract_addr, get_account_addr
from python_sdk.eth_utils import to_checksum_address
from python_sdk.client.bcosclient import BcosClient

def deploy_authentication_contract(fiscoClient):
    smart_contract = SmartContract('Authentication')
    contract_addr = smart_contract.deploy_contract(fiscoClient)
    return contract_addr


# before call this function should deploy the authentication contract first
def deploy_taskMain_contract(fiscoClient):
    smart_contract = SmartContract('TaskMain')
    contract_addr = smart_contract.deploy_contract(fiscoClient)

    # set the authentication contract addr
    authentication_addr = get_contract_addr(smart_contract.get_contract_address_path(), 'Authentication')
    smart_contract.write_data(fiscoClient, 'setAuthenticationContract', [authentication_addr])

    return contract_addr


# before call this function should deploy the authentication contract first
def deploy_model_storage_contract(fiscoClient):
    smart_contract = SmartContract('ModelStorageMain')
    contract_addr = smart_contract.deploy_contract(fiscoClient)

    return contract_addr


# before call this function should deploy the authentication contract first
def deploy_grad_storage_contract(fiscoClient):
    smart_contract = SmartContract('GradStorageMain')
    contract_addr = smart_contract.deploy_contract(fiscoClient)

    return contract_addr


def deploy_trade_token_contract(fiscoClient):
    smart_contract = SmartContract('TradeToken')
    contract_addr = smart_contract.deploy_contract(fiscoClient)

    # transfer some token to some accounts.
    task_publisher = get_account_addr('bin/accounts/accounts.ini', ['task_publisher'])
    worker1 = get_account_addr('bin/accounts/accounts.ini', ['worker1'])
    worker2 = get_account_addr('bin/accounts/accounts.ini', ['worker2'])
    task_publisher_addr = to_checksum_address(task_publisher[0])
    worker1_addr = to_checksum_address(worker1[0])
    worker2_addr = to_checksum_address(worker2[0])
    smart_contract.write_data(fiscoClient, 'set', [task_publisher_addr, 10000000])
    smart_contract.write_data(fiscoClient, 'set', [worker1_addr, 20000000])
    smart_contract.write_data(fiscoClient, 'set', [worker2_addr, 30000000])

    return contract_addr


def deploy_trade_main_contract(fiscoClient):
    smart_contract = SmartContract('TradeMain')
    contract_addr = smart_contract.deploy_contract(fiscoClient)

    token_addr = get_contract_addr(smart_contract.get_contract_address_path(), 'TradeToken')
    smart_contract.write_data(fiscoClient, 'setTokenContract', [token_addr])

    return contract_addr


fiscoClient = BcosClient()

deploy_model_storage_contract(fiscoClient)
deploy_grad_storage_contract(fiscoClient)

# deploy_authentication_contract(fiscoClient)
# deploy_taskMain_contract(fiscoClient)
# deploy_trade_token_contract(fiscoClient)
# deploy_trade_main_contract(fiscoClient)


fiscoClient.finish()