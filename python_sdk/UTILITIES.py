import pickle
import os
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt

class Convertor:
    def __init__(self):
        pass

    def data_to_str(self, data):
        data_bytes = pickle.dumps(data)
        data_str = data_bytes.decode('iso-8859-1')
        return data_str

    def str_to_data(self, data_str):
        data_bytes = data_str.encode('iso-8859-1')
        data = pickle.loads(data_bytes)
        return data

def curr_time():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]


def creat_files(dir, file_names):
    for file_name in file_names:
        file_path = dir + file_name
        if not os.path.exists(file_path):
            os.system(r"touch {}".format(file_path))


def append_data_to_file(file_path, new_data):
    try:
        a = np.loadtxt(file_path)
        merge_data = np.append(a, new_data)
    except:
        pass
    np.savetxt(file_path, merge_data, delimiter=",")


def plot_figures(dir, workers_file_paths):
    plot_time = curr_time()
    plt.figure(1)
    plt.title('Local Losses of Three Workers')
    plt.xlabel('Round')
    plt.ylabel('Loss')
    lengend = []
    for i in range(len(workers_file_paths[0:3])):
        plt.plot(np.loadtxt(dir + workers_file_paths[i]))
        lengend.append((workers_file_paths[i].split(".")[0]).split('_')[0])
    plt.legend(lengend)
    # plt.savefig("figures/losses_of_three_workers_{}.png".format(plot_time))

    plt.figure(2)
    plt.title('Accuracy')
    plt.xlabel('Round')
    plt.ylabel('Accuracy(%)')
    plt.plot(np.loadtxt(dir + workers_file_paths[3]))
    # plt.savefig("figures/{}_{}.png".format(workers_file_paths[3].split(".")[0], plot_time))

    plt.figure(3)
    plt.title('Losses')
    plt.xlabel('Round')
    plt.ylabel('Loss')
    plt.plot(np.loadtxt(dir + workers_file_paths[4]))
    # plt.savefig("figures/{}_{}.png".format(workers_file_paths[4].split(".")[0], plot_time))

    plt.show()