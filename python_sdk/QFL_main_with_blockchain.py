import sys
sys.path.append('../')
import os
import copy
import torch
import torch.nn as nn
from python_sdk.QFL.data_utils import DatasetSplit
from python_sdk.QFL.initialize_data import load_model_and_data_and_userGroup
from torch.utils.data import DataLoader
import pickle
import matplotlib.pyplot as plt
import datetime
import time

import logging
logger = logging.getLogger(__name__)
LOG_INTERVAL = 50


from python_sdk.contract_api import ModelStorage, GradStorage
from python_sdk.UTILITIES import Convertor

from python_sdk.client.bcosclient import BcosClient
import os


seed = 0
torch.manual_seed(seed)
# rn.seed(seed)    #should be command when use the initialize the data
# np.random.seed(seed)   #should be command when use the initialize the data
torch.cuda.manual_seed_all(seed)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

class Arguments:
    def __init__(self):
        self.batch_size = 10
        self.test_batch_size = 100
        self.lr = 0.005
        self.device = 'cpu'
        self.local_epoch = 2
        self.lr_decay = 0.996
        self.momentum = 0.9

        self.iid = True  # iid: True; non-iid: False
        self.num_bit = 32 # number of bits to represent the quantized gradient, when num_bit

        self.num_users = 10
        self.epoch = 2  # training round at this time.
        self.continue_train = False  # If True, it will continue training the last taskid.
        self.start_round = 0

        self.model_type = 'withoutQS'   # withoutQS  withQ  withS  withQS
        self.dataset_name = 'traffic'  # mnist cifar  traffic
        self.net_name = "LeNet"        # CNN          LeNet
        self.log_folder_name = "QFL/log_test_running_time"


class Client:
    dataset = None
    criterion = nn.CrossEntropyLoss()

    def __init__(self, fiscoClient, args, data_idx, client_id):

        self.local_epoch = args.local_epoch
        self.batch_size = args.batch_size
        self.lr = args.lr
        self.lr_decay = args.lr_decay
        self.device = args.device
        self.momentum = args.momentum

        self.trainloader = DataLoader(DatasetSplit(self.dataset, data_idx), 
                                      batch_size=self.batch_size, shuffle=True)
        self.client_id = client_id
        self.num_data = len(data_idx)
        self.fiscoClient = fiscoClient
        self.convertModel = Convertor()
        self.model_storage_contract = ModelStorage()
        self.grad_storage_contract = GradStorage()
        self.convertGradient = Convertor()

    def local_update_upload(self, client_id, num_bit):

        # ==============================================================================
        # Download the new global model from FISCO

        training_round = self.model_storage_contract.get_curr_epoch(self.fiscoClient)
        print('Client {} is training the {}th round'.format(client_id, training_round))
        model_str = self.model_storage_contract.get_model_by_epoch(self.fiscoClient, training_round)
        local_model = self.convertModel.str_to_data(model_str)
        # ==============================================================================

        initial = copy.deepcopy(local_model)  # copy the model distributed from the server
        lr = self.lr * (self.lr_decay ** int(training_round))  # decay the learning rate
        optimizer = torch.optim.SGD(local_model.parameters(), lr=lr, momentum=self.momentum)
        local_model.train()
        for e in range(self.local_epoch):
            for batch_idx, (images, labels) in enumerate(self.trainloader):
                images, labels = images.to(self.device), labels.to(self.device)
                local_model.zero_grad()
                output = local_model(images)
                loss = self.criterion(output, labels)
                loss.backward()
                optimizer.step()

        # ==============================================================================
        gradient = local_model.quantize_grad_with_blockchain(initial, num_bit, fn='grad' + str(self.client_id))
        # upload the quantized grad $G_{t}$ of each client to FISCO.

        grad_str = self.convertGradient.data_to_str(gradient)
        # print("\nGrad_str size: {} KB".format(sys.getsizeof(grad_str) / 1e3))
        self.grad_storage_contract.upload_grad(self.fiscoClient, training_round, grad_str)

        # ==============================================================================

        # print('\rClient %d finished local training at %d communication round' % (self.client_id, int(training_round)), end='')
        return self.num_data
        
class Server:
    def __init__(self, fiscoClient, args, model, clients, testset, log_name):
        self.model_dist = copy.deepcopy(model)
        self.model_glob = copy.deepcopy(self.model_dist)
        self.clients = clients
        self.testloader = DataLoader(testset, batch_size=args.test_batch_size, shuffle=False)
        self.criterion = nn.CrossEntropyLoss()
        self.device = args.device
        self.fiscoClient = fiscoClient
        
        # Note that the global test loss, accuracy and running_time are recored in this demo
        self.his_loss = [] 
        self.his_acc = []
        self.timeslot = []
        
        # path to save the info
        if not os.path.exists(args.log_folder_name):
            os.mkdir(args.log_folder_name)
        self.log_path = args.log_folder_name + log_name
        self.grad_storage_contract = GradStorage()
        self.convertGradient = Convertor()
        self.convertModel = Convertor()

    def _single_client(self, training_round, client_id, num_bit):
        """
        Perform local update to single client at one communication round.
        """
        local_model = copy.deepcopy(self.model_dist)
        num_local_data = self.clients[client_id].local_update_upload(client_id, num_bit)
        # =======================================================================================================
        # get the newest gradient from FISCO

        grad_str = self.grad_storage_contract.get_newest_grad(self.fiscoClient, training_round)
        gradient = self.convertGradient.str_to_data(grad_str)
        local_model.decode_grad_with_blockchain(copy.deepcopy(self.model_dist), gradient,
                                fn='grad' + str(self.clients[client_id].client_id))
        # =======================================================================================================

        self.model_glob.aggregate(local_model, num_local_data)



    def single_cr(self, num_bit):
        """
        Perform local update to all the clients one by one。
        """

        curr = time.time()

        # =======================================================================================================
        model_storage_contract = ModelStorage()
        training_round = model_storage_contract.get_curr_epoch(self.fiscoClient)
        # =======================================================================================================

        # self.model_dist.to(self.device)
        for client_id in range(len(self.clients)):
            self._single_client(training_round, client_id, num_bit)
        self.model_dist = copy.deepcopy(self.model_glob)
        training_round = int(training_round) + 1

        # clear the aggregation data stored in the model
        self.model_dist.zero_agg_data()
        self.model_glob.zero_agg_data()

        # =======================================================================================================

        model_str = self.convertModel.data_to_str(self.model_dist)
        # print("\nModel_str size: {} KB".format(sys.getsizeof(model_str) / 1e3))
        model_storage_contract.upload_model(self.fiscoClient, str(training_round), model_str)
        print('\nFinished upload the global model of {}th round to FISCO'.format(training_round))
        # =======================================================================================================
        upload_time_cost = time.time() - curr
        self.timeslot.append(upload_time_cost)

        # evaluate the global model
        # save the training info in current communication round
        self.model_dist.eval()
        test_loss = 0
        correct = 0
        total = 0
        with torch.no_grad():
            for batch_idx, (images, labels) in enumerate(self.testloader):
                images, labels = images.to(self.device), labels.to(self.device)
                outputs = self.model_dist(images)
                loss = self.criterion(outputs, labels)

                test_loss += loss.item()
                _, predicted = outputs.max(1)
                total += labels.size(0)
                correct += predicted.eq(labels).sum().item()
        acc = 100.*correct/total
        test_loss /= total
        self.his_acc.append(acc)
        self.his_loss.append(test_loss)
        
        # write the log info
        with open(self.log_path, 'wb') as file:
            pickle.dump([self.his_acc, self.his_loss, self.timeslot], file)
        
        print("\nComm round %d, test acc: %.6f, test loss: %.6f" % (training_round, acc, test_loss))
        return acc, test_loss


def main(client):
    start=datetime.datetime.now()
    args = Arguments()
    clients = []

    # when compare with blockchain version, should use the same training data, user_group, and same initialize model file.
    # -------------------------------------------------------------------------------------------------------------------
    file_path = './QFL/init_data/{}_{}_{}_idd{}_num_users{}_initial_data_and_userGroup'.format(args.model_type, args.dataset_name, args.net_name, args.iid, args.num_users)
    train_dataset, test_dataset, user_groups = load_model_and_data_and_userGroup(file_path)
    Client.dataset = train_dataset
    model = torch.load('./QFL/init_data/{}_{}_{}_idd{}_num_users{}_initial_model.pt'.format(args.model_type, args.dataset_name, args.net_name, args.iid, args.num_users))
    # -------------------------------------------------------------------------------------------------------------------
    print(file_path)
    print(args.model_type)
    if args.continue_train:
        model_storage_contract = ModelStorage()
        convertModel = Convertor()
        epoch = model_storage_contract.get_curr_epoch(client)
        model_str = model_storage_contract.get_model_by_epoch(client, epoch)
        last_model = convertModel.str_to_data(model_str)
        model = copy.deepcopy(last_model)
    else:
        model_storage_contract = ModelStorage()
        convertModel = Convertor()
        init_model_str = convertModel.data_to_str(model)
        # upload init model
        model_storage_contract.upload_model(client, "0", init_model_str)
    for user_idx in range(args.num_users):
        clients.append(Client(client, args, user_groups[user_idx], user_idx))

    server = Server(client, args, model, clients, test_dataset,
                    log_name='/{}_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}_4nodes'.format(args.net_name, args.dataset_name,
                                                                                      args.model_type, args.iid,
                                                                                      args.num_bit, args.num_users,
                                                                                      args.lr, args.epoch, args.start_round, (args.start_round + args.epoch -1)))
    for cr in range(args.epoch):
        server.single_cr(args.num_bit)

    end=datetime.datetime.now()
    print('Running time:%s Seconds'%(end-start))

    with open(args.log_folder_name + '/time', 'wb') as file:
        pickle.dump(['{}_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}'.format(args.net_name, args.dataset_name, args.model_type, args.iid, args.num_bit, args.num_users, args.lr, args.epoch, args.start_round, (args.start_round + args.epoch - 1)), 'Running time:%s Seconds'%(end-start)], file)

    data = pickle.load(open(args.log_folder_name + '/{}_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}_4nodes'.format(args.net_name, args.dataset_name, args.model_type, args.iid, args.num_bit, args.num_users, args.lr, args.epoch, args.start_round, (args.start_round + args.epoch - 1)), 'rb'))

    # plt.figure()
    # plt.title('Average Accuracy vs Communication rounds')
    # plt.plot(range(len(data[0])), data[0])
    # plt.ylabel('train_accuracy')
    # plt.savefig(args.log_folder_name + '/{}_accuracy_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}.png'.format(args.net_name, args.dataset_name, args.model_type, args.iid, args.num_bit, args.num_users, args.lr, args.epoch, args.start_round, (args.start_round + args.epoch - 1)))
    #
    # plt.figure()
    # plt.title('Training Loss vs Communication rounds')
    # plt.plot(range(len(data[1])), data[1])
    # plt.ylabel('train_loss')
    # plt.savefig(args.log_folder_name + '/{}_loss_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}.png'.format(args.net_name, args.dataset_name, args.model_type, args.iid, args.num_bit, args.num_users, args.lr, args.epoch, args.start_round, (args.start_round + args.epoch - 1)))

    plt.figure()
    plt.title('Running time vs Communication rounds')
    plt.plot(range(len(data[2])), data[2])
    plt.ylabel('running time')
    plt.savefig(args.log_folder_name + '/{}_time_{}_{}_iid{}_{}num_bit_{}num_users_{}lr_{}epoch{}to{}_total_time_4nodes.png'.format(args.net_name, args.dataset_name, args.model_type, args.iid, args.num_bit, args.num_users, args.lr, args.epoch, args.start_round, (args.start_round + args.epoch - 1)))


if __name__ == "__main__":
    client = BcosClient()
    main(client)
    client.finish()
