pragma solidity>=0.4.24 <0.6.11;
pragma experimental ABIEncoderV2;
import "./Table.sol";

contract GradStorageMain {


    string constant TABLE_NAME = "gradient_table_task_6";
    address public contractOwner;
    TableFactory tableFactory;

    mapping(string => int256) newestGradIdOfEpoch;

    constructor() public {
        tableFactory = TableFactory(0x1001);
        contractOwner = msg.sender;
        tableFactory.createTable(TABLE_NAME,"epoch"," timestamp, uploader_address, gradient_string, gradient_id");
    }


    function getNewestGradIdOfTask(string memory epoch) internal view returns(int256){
        int256 gradId = newestGradIdOfEpoch[epoch];
        return gradId;

    }


    function recordGradient(address myAddr, string memory epoch, string memory gradStr) public returns (int256)
    {

        int256 gradId = getNewestGradIdOfTask(epoch);
        int256 newGradId = gradId + 1;

        Table table = tableFactory.openTable(TABLE_NAME);
        Entry entry = table.newEntry();
        entry.set("epoch", epoch);
        entry.set("timestamp", now);
        entry.set("uploader_address", myAddr);
        entry.set("gradient_string", gradStr);
        entry.set("gradient_id", newGradId);
        int256 count = table.insert(epoch, entry);
        newestGradIdOfEpoch[epoch] = newGradId;
        return count;
    }


    function getNewestGradient(string memory epoch) public returns (string[] memory) {
        int256 gradId = getNewestGradIdOfTask(epoch);

        Table table = tableFactory.openTable(TABLE_NAME);
        Condition condition = table.newCondition();
        condition.EQ("epoch", epoch);
        condition.EQ("gradient_id", gradId);

        Entries entries = table.select(epoch, condition);
        string[] memory grad_string_list = new string[](uint256(entries.size()));

        for (int256 i = 0; i < entries.size(); ++i) {
            Entry entry = entries.get(i);
            grad_string_list[uint256(i)] = entry.getString("gradient_string");
        }

        return grad_string_list;
    }

}
