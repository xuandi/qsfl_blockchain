pragma solidity>=0.4.24 <0.6.11;
pragma experimental ABIEncoderV2;

import "./ParallelContract.sol";  
import "./SafeMath.sol";


// make ParallelContract as the base class
contract TradeToken is ParallelContract {

    using SafeMath for uint256;

    // contract realization
    mapping (address => uint256) _balance;

    function transfer(address from, address to, uint256 num) public {
        _balance[from]=_balance[from].sub(num);
        _balance[to] = _balance[to].add(num);
    }

    function set(address addr, uint256 num) public {
        _balance[addr] = num;
    }

    function balanceOf(address addr) public view returns (uint256) {
        return _balance[addr];
    }


    // register parallel contract interface
    function enableParallel() public {
        // function defined character string (no blank space behind ","), the former part of parameter constitutes exclusive parameter (which should be put ahead when designing function)
        registerParallelFunction("transfer(string,string,uint256)", 2); // critical: string string
        registerParallelFunction("set(string,uint256)", 1); // critical: string
    }


    // revoke parallel contract interface
    function disableParallel() public
    {
        unregisterParallelFunction("transfer(string,string,uint256)");
        unregisterParallelFunction("set(string,uint256)");
    }
}
