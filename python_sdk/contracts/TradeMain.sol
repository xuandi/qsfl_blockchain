pragma solidity>=0.4.24 <0.6.11;

pragma experimental ABIEncoderV2;
import "./Table.sol";
import "./DataFormat.sol";
import "./TradeToken.sol";

contract TradeMain is DataFormat {

    TradeToken public tradeToken;

    string constant TABLE_NAME = "orders_table_V4";
    mapping(string => bool) onSaleStatusOfTaskId;

    TableFactory tableFactory;

    constructor() public {
        tableFactory = TableFactory(0x1001);
        tableFactory.createTable(TABLE_NAME,"task_id","seller_address, model_type, model_price, buyer_address_list, sell_time_list");
    }


    function setTokenContract( address tokenAddr) public {
        tradeToken = TradeToken(tokenAddr);
    }

    function getOnSaleStatusOfTaskId(string memory taskId) public view returns(bool){
        return onSaleStatusOfTaskId[taskId];
    }


    function createOrder(address myAddr, string memory taskId, string memory modelType, uint modelPrice) public returns(int256) {

        Table table = tableFactory.openTable(TABLE_NAME);
        string memory str;
        string memory priceStr = uintToString(modelPrice);
        Entry entry = table.newEntry();
        entry.set("task_id", taskId);
        entry.set("seller_address", myAddr);
        entry.set("model_type", modelType);
        entry.set("model_price", priceStr);  //table doesn't support to getUInt, so need to change to string type.
        entry.set("buyer_address_list", str);
        entry.set("sell_time_list", str);

        onSaleStatusOfTaskId[taskId] = true; //true means task is onSale

        int256 count = table.insert(taskId, entry);
        return count;
    }


    function updateOrderPrice(address myAddr, string memory taskId, uint newModelPrice) public returns(int256) {
        (address sellerAddr, , , ) = getOrderByTaskId(taskId);
        require(sellerAddr == myAddr);
        Table table = tableFactory.openTable(TABLE_NAME);
        string memory priceStr = uintToString(newModelPrice);
        Entry entry = table.newEntry();

        entry.set("model_price", priceStr);  //table doesn't support to getUInt, so need to change to string type.
        Condition condition = table.newCondition();
        condition.EQ("task_id", taskId);


        int256 count = table.update(taskId, entry, condition);
        return count;
    }
    
    function purchaseModel_revert(string memory myAddr, string memory taskId) public returns(int256) {

        Table table = tableFactory.openTable(TABLE_NAME);

        Entry entry = table.newEntry();

        (address sellerAddr, uint256 price, string memory buyerStr, string memory timeStr) = getOrderByTaskId(taskId);
        string memory newAddrListStr = trimConcat(buyerStr, myAddr);
        string memory newTimeListStr = trimConcat(timeStr, uintToString(now));

        entry.set("buyer_address_list", newAddrListStr);
        entry.set("sell_time_list", newTimeListStr);

        Condition condition = table.newCondition();
        condition.EQ("task_id", taskId);

        int256 count = table.update(taskId, entry, condition);
        address _addr = toAddress(myAddr);
        tradeToken.transfer(sellerAddr, _addr, price);

        return count;
    }


    function purchaseModel(string memory myAddr, string memory taskId) public returns(int256) {
        require(onSaleStatusOfTaskId[taskId] == true);


        Table table = tableFactory.openTable(TABLE_NAME);

        Entry entry = table.newEntry();

        (address sellerAddr, uint256 price, string memory buyerStr, string memory timeStr) = getOrderByTaskId(taskId);
        string memory newAddrListStr = strConcat(buyerStr, myAddr);
        string memory newTimeListStr = strConcat(timeStr, uintToString(now));

        entry.set("buyer_address_list", newAddrListStr);
        entry.set("sell_time_list", newTimeListStr);

        Condition condition = table.newCondition();
        condition.EQ("task_id", taskId);

        int256 count = table.update(taskId, entry, condition);
        address _addr = toAddress(myAddr);
        require(tradeToken.balanceOf(_addr) >= price);
        tradeToken.transfer(_addr, sellerAddr, price);

        return count;
    }
    
    
    


    function getOrderByTaskId(string memory taskId) public returns(address, uint256, string memory, string memory) {
        Table table = tableFactory.openTable(TABLE_NAME);

        Condition condition = table.newCondition();
        condition.EQ("task_id", taskId);

        Entries entries = table.select(taskId, condition);
        address[] memory seller_address_list = new address[](uint256(entries.size()));
        string[] memory model_price_list = new string[](uint256(entries.size()));
        string[] memory buyer_address_list = new string[](uint256(entries.size()));
        string[] memory sell_time_list = new string[](uint256(entries.size()));

        for (int256 i = 0; i < entries.size(); ++i) {
            Entry entry = entries.get(i);
            seller_address_list[uint256(i)] = entry.getAddress("seller_address");
            model_price_list[uint256(i)] = entry.getString("model_price");
            buyer_address_list[uint256(i)] = entry.getString("buyer_address_list");
            sell_time_list[uint256(i)] = entry.getString("sell_time_list");
        }

        return (seller_address_list[0], stringToUint(model_price_list[0]), buyer_address_list[0], sell_time_list[0]);
    }


}

