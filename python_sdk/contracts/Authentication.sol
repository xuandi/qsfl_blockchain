pragma solidity>=0.4.24 <0.6.11;


contract Authentication {

    mapping(string => mapping(address => bool)) permissionOfTaskId;
    mapping(string => address) publisherOfTaskId;

    function approve (string memory taskId, address newAddr) public returns(bool) {
       permissionOfTaskId[taskId][newAddr] = true;
        return permissionOfTaskId[taskId][newAddr];
    }

    function approve_revert (string memory taskId, address newAddr) public returns(bool) {
        permissionOfTaskId[taskId][newAddr] = false;
        return permissionOfTaskId[taskId][newAddr];
    }

    function checkPermissionByAddr(string memory taskId, address addr) public view returns(bool) {
        return permissionOfTaskId[taskId][addr];
    }

    function getTaskPublisher(string memory taskId) public view returns(address) {
        return publisherOfTaskId[taskId];
    }

    function setTaskPublisher(string memory taskId, address addr) public {
        publisherOfTaskId[taskId] = addr;
    }

}


