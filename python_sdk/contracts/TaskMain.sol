pragma solidity>=0.4.24 <0.6.11;
pragma experimental ABIEncoderV2;
import "./Table.sol";
import "./DataFormat.sol";
import "./Authentication.sol";

contract TaskMain is DataFormat {

    TableFactory tableFactory;
    string constant TABLE_NAME = "tasks_table33";
    Authentication public authentication;

    uint256 public maxtaskId;

    constructor() public {
        maxtaskId = 0;
        tableFactory = TableFactory(0x1001);
        tableFactory.createTable(TABLE_NAME,"task_id","task_publisher, workers, epochNum, batch_size, lr, start_time, end_time, status");  //task_status: start->1 done->2 cancel->0
    }

    function setAuthenticationContract( address authenticationAddr) public {
        authentication = Authentication(authenticationAddr);
    }

    function getCurrentTaskId() view public returns (uint256) {
        return maxtaskId;
    }

    //add task and start task
    function addTask(string memory taskId,  address[] memory workers, int256 epochNum, int256 batchSize, int256 lr) public returns (uint256)  //onlyTask owner allow to call this function
    {
        string memory workersAddrStr = addrArrayToStr(workers);

        Table table = tableFactory.openTable(TABLE_NAME);

        Entry entry = table.newEntry();
        entry.set("task_id", taskId);
        entry.set("task_publisher", toString(msg.sender));
        entry.set("workers", workersAddrStr);
        entry.set("epochNum", epochNum);
        entry.set("batch_size", batchSize);
        entry.set("lr", lr);
        entry.set("start_time", now);
        entry.set("end_time", uint(0));
        entry.set("status", uint(1));     //1 means task is ready
        table.insert(taskId, entry);

        maxtaskId += 1;
        authentication.setTaskPublisher(taskId, msg.sender);
        authentication.approve(taskId, msg.sender);

        for (uint i = 0; i < workers.length; i++) {
            authentication.approve(taskId, workers[i]);
        }

        return maxtaskId;
    }



    function getTasksByTaskId(string memory taskId) public returns (string[] memory, string[] memory, int256[] memory, int256[] memory) {
        Table table = tableFactory.openTable(TABLE_NAME);

        Condition condition = table.newCondition();
        condition.EQ("task_id", taskId);

        Entries entries = table.select(taskId, condition);
        string[] memory task_publisher_list = new string[](uint256(entries.size()));
        string[] memory workers_list = new string[](uint256(entries.size()));
        int256[] memory lr_list = new int256[](uint256(entries.size()));
        int256[] memory status_list = new int256[](uint256(entries.size()));

        for (int256 i = 0; i < entries.size(); ++i) {
            Entry entry = entries.get(i);
            task_publisher_list[uint256(i)] = entry.getString("task_publisher");
            workers_list[uint256(i)] = entry.getString("workers");
            lr_list[uint256(i)] = entry.getInt("status");
            status_list[uint256(i)] = entry.getInt("status");
        }

        return (task_publisher_list, workers_list, status_list, lr_list);

    }


    //close the task when the task is done. and will call the transfer function to deposit token from task owner in trade chain via wecross/
    function closeTask(string memory taskId, address[] memory workers) public returns (int256) {   //onlyTask owner allow to call this function
        require(authentication.getTaskPublisher(taskId) == msg.sender);
        Table table = tableFactory.openTable(TABLE_NAME);

        Entry entry = table.newEntry();
        entry.set("end_time", now);
        entry.set("status", int(2));  // 2 means closed.


        Condition condition = table.newCondition();
        condition.EQ("task_id", taskId);
        condition.EQ("task_publisher", toString(msg.sender));

        int256 count = table.update(taskId, entry, condition);

        for (uint i = 0; i < workers.length; i++) {
            authentication.approve_revert(taskId,workers[i]);
        }

        return count;
    }


    //remove records
    function removeTask(string memory taskId, address[] memory workers) public returns (int256) {     //onlyTask owner allow to call this function
        Table table = tableFactory.openTable(TABLE_NAME);

        Condition condition = table.newCondition();
        condition.EQ("task_id", taskId);
        condition.EQ("task_publisher", toString(msg.sender));

        int256 count = table.remove(taskId, condition);

        for (uint i = 0; i < workers.length; i++) {
            authentication.approve_revert(taskId,workers[i]);
        }

        return count;
    }
}
