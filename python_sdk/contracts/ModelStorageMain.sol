pragma solidity ^0.4.25;
pragma experimental ABIEncoderV2;
import "./Table.sol";


contract ModelStorageMain {

    string constant TABLE_NAME = "model_table_task_6";
    address public contractOwner;

    TableFactory tableFactory;
    uint public nextEpochNum;


    constructor() public {
        tableFactory = TableFactory(0x1001);
        contractOwner = msg.sender;
        nextEpochNum = 0;
        tableFactory.createTable(TABLE_NAME,"epoch","timestamp, uploader_address, model_string");
    }


    function getCurrEpochNum() public view returns(uint){
        return (nextEpochNum - 1) ;

    }


    //Insert records: both worker and publisher can call this function,publisher will upload global and worker will upload the local model.
    function recordModel(address myAddr, string memory epoch, string memory modelStr) public returns (int256)
    {
        Table table = tableFactory.openTable(TABLE_NAME);
        Entry entry = table.newEntry();
        entry.set("epoch", epoch);
        entry.set("timestamp", now);
        entry.set("uploader_address", myAddr);
        entry.set("model_string", modelStr);
        int256 count = table.insert(epoch, entry);
        nextEpochNum = nextEpochNum + 1;
        return count;
    }


    //Task publisher call this function to get all the local models of the latest epoch; worker call this function the get global model.
    function getModelByEpoch(string memory epoch) public returns (string[]) {

        Table table = tableFactory.openTable(TABLE_NAME);
        Condition condition = table.newCondition();
        condition.EQ("epoch", epoch);

        Entries entries = table.select(epoch, condition);
        string[] memory model_string_list = new string[](uint256(entries.size()));

        for (int256 i = 0; i < entries.size(); ++i) {
            Entry entry = entries.get(i);
            model_string_list[uint256(i)] = entry.getString("model_string");
        }
        return model_string_list;
    }



}
